#ifndef __BINARY_C_PROTOCOL_TYPES_H__
#define __BINARY_C_PROTOCOL_TYPES_H__
#include <binary_c.h>
#include <zoo/animal.h>
#ifdef __cplusplus
extern "C"
{
#endif

	typedef struct
	{
		int16_t int16; // 1
		int32_t int32; // 2
		int64_t int64; // 3
		uint16_t uint16; // 4
		uint32_t uint32; // 5
		uint64_t uint64; // 6
		BINARY_C_FLOAT32_T float32; // 7
		BINARY_C_FLOAT64_T float64; // 8
		uint8_t b; // 9
		BINARY_C_BOOL ok; // 10
		binary_c_string_t str; // 11
		binary_c_interface_t cat; // 12
		binary_c_interface_t dog; // 13
		int16_t pos; // 14
		binary_c_array_t int16s; // 21
		binary_c_array_t int32s; // 22
		binary_c_array_t int64s; // 23
		binary_c_array_t uint16s; // 24
		binary_c_array_t uint32s; // 25
		binary_c_array_t uint64s; // 26
		binary_c_array_t float32s; // 27
		binary_c_array_t float64s; // 28
		binary_c_array_t bs; // 29
		binary_c_bits_t oks; // 30
		binary_c_array_t strs; // 31
		binary_c_array_t cats; // 32
		binary_c_array_t dogs; // 33
		binary_c_array_t poss; // 34
	} binary_c_types_types_t, *binary_c_types_types_pt;

	void binary_c_types_types_reset(binary_c_types_types_pt msg);
	void binary_c_types_types_free(binary_c_free_f free, binary_c_types_types_pt msg);
	int binary_c_types_types_marshal_size(binary_c_context_pt ctx, binary_c_types_types_pt msg, int use);
	int binary_c_types_types_marshal(binary_c_context_pt ctx, binary_c_types_types_pt msg, uint8_t *buffer, int capacity, int use);
	int binary_c_types_types_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use);
	int binary_c_types_types_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_types_types_pt msg);
	void binary_c_types_types_cat_set(binary_c_types_types_pt msg, binary_c_zoo_animal_cat_pt val);
	binary_c_zoo_animal_cat_pt binary_c_types_types_cat_get(binary_c_types_types_pt msg);
	void binary_c_types_types_dog_set(binary_c_types_types_pt msg, binary_c_zoo_animal_dog_pt val);
	binary_c_zoo_animal_dog_pt binary_c_types_types_dog_get(binary_c_types_types_pt msg);
	void binary_c_types_types_int16s_set(binary_c_types_types_pt msg, int i, int16_t val);
	int16_t binary_c_types_types_int16s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_int32s_set(binary_c_types_types_pt msg, int i, int32_t val);
	int32_t binary_c_types_types_int32s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_int64s_set(binary_c_types_types_pt msg, int i, int64_t val);
	int64_t binary_c_types_types_int64s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_uint16s_set(binary_c_types_types_pt msg, int i, uint16_t val);
	uint16_t binary_c_types_types_uint16s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_uint32s_set(binary_c_types_types_pt msg, int i, uint32_t val);
	uint32_t binary_c_types_types_uint32s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_uint64s_set(binary_c_types_types_pt msg, int i, uint64_t val);
	uint64_t binary_c_types_types_uint64s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_float32s_set(binary_c_types_types_pt msg, int i, BINARY_C_FLOAT32_T val);
	BINARY_C_FLOAT32_T binary_c_types_types_float32s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_float64s_set(binary_c_types_types_pt msg, int i, BINARY_C_FLOAT64_T val);
	BINARY_C_FLOAT64_T binary_c_types_types_float64s_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_bs_set(binary_c_types_types_pt msg, int i, uint8_t val);
	uint8_t binary_c_types_types_bs_get(binary_c_types_types_pt msg, int i);
	int binary_c_types_types_oks_set(binary_c_types_types_pt msg, int i, BINARY_C_BOOL val);
	BINARY_C_BOOL binary_c_types_types_oks_get(binary_c_types_types_pt msg,int i);
	void binary_c_types_types_strs_set(binary_c_types_types_pt msg, int i, binary_c_string_t val);
	binary_c_string_t binary_c_types_types_strs_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_cats_set(binary_c_types_types_pt msg, int i, binary_c_zoo_animal_cat_pt val);
	binary_c_zoo_animal_cat_pt binary_c_types_types_cats_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_dogs_set(binary_c_types_types_pt msg, int i, binary_c_zoo_animal_dog_pt val);
	binary_c_zoo_animal_dog_pt binary_c_types_types_dogs_get(binary_c_types_types_pt msg, int i);
	void binary_c_types_types_poss_set(binary_c_types_types_pt msg, int i, int16_t val);
	int16_t binary_c_types_types_poss_get(binary_c_types_types_pt msg, int i);

#ifdef __cplusplus
}
#endif

#endif // __BINARY_C_PROTOCOL_TYPES_H__
