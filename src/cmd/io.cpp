#include "cmd.h"

#include "./types.h"
#include <boost/smart_ptr.hpp>

void subcommand_io(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("io", "io test");
    cmd->callback([&cmd = *cmd] {
        try
        {
            const std::string w = cmd.get_option("--write")->as<std::string>();
            const std::string r = cmd.get_option("--read")->as<std::string>();
            if (!w.empty())
            {
                binary_c_context_t ctx = binary_c_background();
                binary_c_types_types_t t;
                set_types(t);
                int marshal_size = binary_c_types_types_marshal_size(&ctx, &t, 0);
                boost::shared_array<uint8_t> data(new uint8_t[marshal_size]);
                int ok = binary_c_types_types_marshal(&ctx, &t, data.get(), marshal_size, 0);
                if (BINARY_C_HAS_ERROR(ok))
                {
                    std::cerr << "binary_c_types_types_marshal " << ok << std::endl;
                    throw CLI::RuntimeError(ok);
                }

                std::ofstream f(w.c_str(), std::ios::trunc | std::ios::out | std::ios::binary);
                f.write((const char *)data.get(), marshal_size);
                if (!f)
                {
                    std::cout << "write error" << std::endl;
                    throw CLI::RuntimeError(-1);
                }
            }
            else if (!r.empty())
            {
                puts("read");
                std::ifstream f(r.c_str(), std::ios::in | std::ios::binary);
                f.seekg(0, std::ios::end);
                int n = f.tellg();
                std::cout << n << "\n";
                boost::shared_array<uint8_t> data(new uint8_t[n]);
                f.seekg(0, std::ios::beg);
                f.read((char *)data.get(), n);

                binary_c_context_t ctx = binary_c_background();
                binary_c_types_types_t t;
                binary_c_allocator_t allocator = binary_c_make_dynamic_allocator(malloc, free);
                int ok = binary_c_types_types_unmarshal(&ctx, &allocator, data.get(), n, &t);
                if (BINARY_C_HAS_ERROR(ok))
                {
                    std::cerr << "binary_c_types_types_unmarshal " << ok << std::endl;
                    throw CLI::RuntimeError(ok);
                }
                display_types(&t);
                binary_c_types_types_free(free, &t);
            }
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << std::endl;
            throw CLI::RuntimeError(-1);
        }
        throw CLI::Success();
    });
    static std::string w, r;
    cmd->add_option("-w,--write",
                    w,
                    "write file",
                    false);
    cmd->add_option("-r,--read",
                    r,
                    "read file",
                    false);
}