#include "binary_c.h"

uint16_t binary_c_internal_uint16(const uint8_t *data)
{
    return *((uint16_t *)data);
}
uint16_t binary_c_internal_uint16_different(const uint8_t *data)
{
    return (uint16_t)data[0] << 8 |
           (uint16_t)data[1];
}
void binary_c_internal_put_uint16(uint8_t *data, uint16_t val)
{
    *((uint16_t *)data) = val;
}
void binary_c_internal_put_uint16_different(uint8_t *data, uint16_t val)
{
    data[0] = (uint8_t)(val >> 8);
    data[1] = (uint8_t)(val & 0xff);
}
uint32_t binary_c_internal_uint32(const uint8_t *data)
{
    return *((uint32_t *)data);
}
uint32_t binary_c_internal_uint32_different(const uint8_t *data)
{
    return (uint32_t)data[0] << 24 |
           (uint32_t)data[1] << 16 |
           (uint32_t)data[2] << 8 |
           (uint32_t)data[3];
}
void binary_c_internal_put_uint32(uint8_t *data, uint32_t val)
{
    *((uint32_t *)data) = val;
}
void binary_c_internal_put_uint32_different(uint8_t *data, uint32_t val)
{
    data[0] = (uint8_t)(val >> 24);
    data[1] = (uint8_t)(val >> 16);
    data[2] = (uint8_t)(val >> 8);
    data[3] = (uint8_t)(val);
}
uint64_t binary_c_internal_uint64(const uint8_t *data)
{
    return *((uint64_t *)data);
}
uint64_t binary_c_internal_uint64_different(const uint8_t *data)
{
    return (uint64_t)data[0] << 56 |
           (uint64_t)data[1] << 48 |
           (uint64_t)data[2] << 40 |
           (uint64_t)data[3] << 32 |
           (uint64_t)data[4] << 24 |
           (uint64_t)data[5] << 16 |
           (uint64_t)data[6] << 8 |
           (uint64_t)data[7];
}

void binary_c_internal_put_uint64(uint8_t *data, uint64_t val)
{
    *((uint64_t *)data) = val;
}
void binary_c_internal_put_uint64_different(uint8_t *data, uint64_t val)
{
    data[0] = (uint8_t)(val >> 56);
    data[1] = (uint8_t)(val >> 48);
    data[2] = (uint8_t)(val >> 40);
    data[3] = (uint8_t)(val >> 32);
    data[4] = (uint8_t)(val >> 24);
    data[5] = (uint8_t)(val >> 16);
    data[6] = (uint8_t)(val >> 8);
    data[7] = (uint8_t)(val);
}
void binary_c_byte_order(binary_c_byte_order_pt order, BINARY_C_BOOL different)
{
    if (different)
    {
        order->uint16 = binary_c_internal_uint16_different;
        order->uint32 = binary_c_internal_uint32_different;
        order->uint64 = binary_c_internal_uint64_different;
        order->put_uint16 = binary_c_internal_put_uint16_different;
        order->put_uint32 = binary_c_internal_put_uint32_different;
        order->put_uint64 = binary_c_internal_put_uint64_different;
    }
    else
    {
        order->uint16 = binary_c_internal_uint16;
        order->uint32 = binary_c_internal_uint32;
        order->uint64 = binary_c_internal_uint64;
        order->put_uint16 = binary_c_internal_put_uint16;
        order->put_uint32 = binary_c_internal_put_uint32;
        order->put_uint64 = binary_c_internal_put_uint64;
    }
}
/**
 * 初始化 小端序列
 */
void binary_c_little_endian(binary_c_byte_order_pt order)
{
    if (0x100 >> 8)
    {
        binary_c_byte_order(order, BINARY_C_FALSE);
    }
    else
    {
        binary_c_byte_order(order, BINARY_C_TRUE);
    }
}
/**
 * 初始化 大端序列
 */
void binary_c_big_endian(binary_c_byte_order_pt order)
{
    if (0x100 >> 8)
    {
        binary_c_byte_order(order, BINARY_C_TRUE);
    }
    else
    {
        binary_c_byte_order(order, BINARY_C_FALSE);
    }
}

binary_c_context_t binary_c_background()
{
    binary_c_context_t ctx;
    ctx.max_buffer = BINARY_C_DEFAULT_MAX_BUFFER;
    binary_c_little_endian(&ctx.byte_order);
    return ctx;
}

binary_c_string_t binary_c_make_const_string(const char *str)
{
    binary_c_string_t s;
    if (str)
    {
        s.data = (char *)str;
        s.length = strlen(str);
        s.capacity = 0;
    }
    else
    {
        s.data = NULL;
        s.length = 0;
        s.capacity = 0;
    }
    return s;
}
binary_c_string_t binary_c_make_string(char *str)
{
    binary_c_string_t s;
    if (str)
    {
        s.data = (char *)str;
        s.length = strlen(str);
        s.capacity = s.length;
    }
    else
    {
        s.data = NULL;
        s.length = 0;
        s.capacity = 0;
    }
    return s;
}
BINARY_C_BOOL binary_c_is_string_equal(binary_c_string_pt s0, binary_c_string_pt s1)
{
    if (s0 && s1)
    {
        if (s0->length != s1->length)
        {
            return BINARY_C_FALSE;
        }
        if (s0->length && memcmp(s0->data, s1->data, s0->length))
        {
            return BINARY_C_FALSE;
        }
        return BINARY_C_TRUE;
    }
    return !s0 && !s1;
}
BINARY_C_BOOL binary_c_bits_get(binary_c_bits_pt bits, int i)
{
    return binary_c_bit_get(bits->data[i / 8], i % 8);
}
BINARY_C_BOOL binary_c_bit_get(uint8_t val, int i)
{
    switch (i)
    {
    case 0:
        val &= 1;
        break;
    case 1:
        val &= 2;
        break;
    case 2:
        val &= 4;
        break;
    case 3:
        val &= 8;
        break;
    case 4:
        val &= 16;
        break;
    case 5:
        val &= 32;
        break;
    case 6:
        val &= 64;
        break;
    case 7:
        val &= 128;
        break;
    }
    return val;
}
int binary_c_bits_set(binary_c_bits_pt bits, int i, BINARY_C_BOOL ok)
{
    return binary_c_bit_set(&(bits->data[i / 8]), i % 8, ok);
}
int binary_c_bit_set(uint8_t *val, int i, BINARY_C_BOOL ok)
{
    int result = BINARY_C_OK;
    switch (i)
    {
    case 0:
        if (ok)
        {
            *val |= 1;
        }
        else
        {
            *val &= ~1;
        }
        break;
    case 1:
        if (ok)
        {
            *val |= 2;
        }
        else
        {
            *val &= ~2;
        }
        break;
    case 2:
        if (ok)
        {
            *val |= 4;
        }
        else
        {
            *val &= ~4;
        }
        break;
    case 3:
        if (ok)
        {
            *val |= 8;
        }
        else
        {
            *val &= ~8;
        }
        break;
    case 4:
        if (ok)
        {
            *val |= 16;
        }
        else
        {
            *val &= ~16;
        }
        break;
    case 5:
        if (ok)
        {
            *val |= 32;
        }
        else
        {
            *val &= ~32;
        }
        break;
    case 6:
        if (ok)
        {
            *val |= 64;
        }
        else
        {
            *val &= ~64;
        }
        break;
    case 7:
        if (ok)
        {
            *val |= 128;
        }
        else
        {
            *val &= ~128;
        }
        break;
    }
    return result;
}
binary_c_allocator_t binary_c_make_static_allocator(binary_c_array_pt buffer)
{
    binary_c_allocator_t allocator;
    allocator.malloc = NULL;
    allocator.free = NULL;
    if (buffer)
    {
        allocator.buffer.data = buffer->data;
        allocator.buffer.length = buffer->length;
        allocator.buffer.capacity = buffer->capacity;
    }
    else
    {
        allocator.buffer.data = NULL;
        allocator.buffer.length = 0;
        allocator.buffer.capacity = 0;
    }
    return allocator;
}
binary_c_allocator_t binary_c_make_static_allocator2(binary_c_interface_t buffer, size_t capacity)
{
    binary_c_allocator_t allocator;
    allocator.malloc = NULL;
    allocator.free = NULL;

    allocator.buffer.data = buffer;
    allocator.buffer.capacity = capacity;
    allocator.buffer.length = 0;
    return allocator;
}
binary_c_allocator_t binary_c_make_dynamic_allocator(binary_c_malloc_f malloc, binary_c_free_f free)
{
    binary_c_allocator_t allocator;
    allocator.malloc = malloc;
    allocator.free = free;
    allocator.buffer.data = NULL;
    allocator.buffer.length = 0;
    allocator.buffer.capacity = 0;
    return allocator;
}
binary_c_interface_t binary_c_malloc(binary_c_allocator_pt allocator, size_t size)
{
    binary_c_interface_t p;
    if (allocator->malloc)
    {
        p = allocator->malloc(size);
    }
    else if (allocator->buffer.capacity)
    {
        if (allocator->buffer.length + size > allocator->buffer.capacity)
        {
            p = NULL;
        }
        else
        {
            p = (char *)(allocator->buffer.data) + allocator->buffer.length;
            allocator->buffer.length += size;
        }
    }
    else
    {
        p = NULL;
    }
    return p;
}
void binary_c_free(binary_c_allocator_pt allocator, binary_c_interface_t p, int length)
{
    if (allocator->malloc)
    {
        if (allocator->free)
        {
            allocator->free(p);
        }
    }
    else if (allocator->buffer.capacity)
    {
        if (allocator->buffer.length > 0 && allocator->buffer.length != length)
        {
            allocator->buffer.length = length;
        }
    }
}

void binary_c_free_string(binary_c_free_f free, binary_c_string_pt str)
{
    if (str)
    {
        if (str->data)
        {
            if (free && str->capacity > 0)
            {
                free(str->data);
            }
            str->data = NULL;
        }
        str->capacity = 0;
        str->length = 0;
    }
}
void binary_c_free_strings(binary_c_free_f free, binary_c_array_pt strs)
{
    if (strs)
    {
        if (strs->data)
        {
            if (free)
            {
                if (strs->capacity > 0 && strs->length > 0)
                {
                    int i = 0;
                    binary_c_string_pt str;
                    for (; i < strs->length; i++)
                    {
                        str = &(((binary_c_string_pt)(strs->data))[i]);
                        binary_c_free_string(free, str);
                    }
                }
                free(strs->data);
            }
            strs->data = NULL;
        }
        strs->capacity = 0;
        strs->length = 0;
    }
}
void binary_c_free_array(binary_c_free_f free, binary_c_array_pt arrs)
{
    if (arrs)
    {
        if (arrs->data)
        {
            if (free)
            {
                free(arrs->data);
            }
            arrs->data = NULL;
        }
        arrs->capacity = 0;
        arrs->length = 0;
    }
}
void binary_c_free_bits(binary_c_free_f free, binary_c_bits_pt bits)
{
    if (bits)
    {
        if (bits->data)
        {
            if (free)
            {
                free(bits->data);
            }
            bits->data = NULL;
        }
        bits->capacity = 0;
        bits->length = 0;
    }
}
void binary_c_free_class_array(binary_c_free_class_f free_class, binary_c_free_f free, binary_c_array_pt arrs)
{
    if (arrs)
    {
        if (arrs->data)
        {
            if (free)
            {
                if (free_class && arrs->capacity > 0 && arrs->length > 0)
                {
                    int i = 0;
                    binary_c_interface_t p;
                    for (; i < arrs->length; i++)
                    {
                        p = ((binary_c_interface_t *)(arrs->data))[i];
                        if (p)
                        {
                            free_class(free, p);
                            free(p);
                        }
                    }
                }
                free(arrs->data);
            }
            arrs->data = NULL;
        }
        arrs->capacity = 0;
        arrs->length = 0;
    }
}
void binary_c_free_class(binary_c_free_class_f free_class, binary_c_allocator_pt allocator, int length, binary_c_interface_t p)
{
    if (allocator->malloc)
    {
        free_class(allocator->free, p);
    }
    else
    {
        if (allocator->buffer.capacity)
        {
            allocator->buffer.length = length;
        }
        free_class(NULL, p);
    }
}
int binary_c_marshal_size_add(binary_c_context_pt ctx, int use, int add)
{
    if (add > BINARY_C_MAX_UINT16)
    {
        return BINARY_C_ERROR_MAX_BUFFER;
    }
    else if (add > ctx->max_buffer)
    {
        return BINARY_C_ERROR_MESSAGE_TOO_LONG;
    }

    use += add;
    if (use > BINARY_C_MAX_UINT16)
    {
        return BINARY_C_ERROR_MAX_BUFFER;
    }
    else if (use > ctx->max_buffer)
    {
        return BINARY_C_ERROR_MESSAGE_TOO_LONG;
    }

    return use;
}
int binary_c_marshal_int16_size(binary_c_context_pt ctx, int use, int16_t v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2);
    }
    return use;
}
int binary_c_marshal_int32_size(binary_c_context_pt ctx, int use, int32_t v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 4);
    }
    return use;
}
int binary_c_marshal_int64_size(binary_c_context_pt ctx, int use, int64_t v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 8);
    }
    return use;
}
int binary_c_marshal_uint16_size(binary_c_context_pt ctx, int use, uint16_t v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2);
    }
    return use;
}
int binary_c_marshal_uint32_size(binary_c_context_pt ctx, int use, uint32_t v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 4);
    }
    return use;
}
int binary_c_marshal_uint64_size(binary_c_context_pt ctx, int use, uint64_t v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 8);
    }
    return use;
}
int binary_c_marshal_float32_size(binary_c_context_pt ctx, int use, BINARY_C_FLOAT32_T v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 4);
    }
    return use;
}
int binary_c_marshal_float64_size(binary_c_context_pt ctx, int use, BINARY_C_FLOAT64_T v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 8);
    }
    return use;
}
int binary_c_marshal_byte_size(binary_c_context_pt ctx, int use, uint8_t v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 1);
    }
    return use;
}
int binary_c_marshal_bool_size(binary_c_context_pt ctx, int use, BINARY_C_BOOL v)
{
    if (v)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 1);
    }
    return use;
}
int binary_c_marshal_string_size(binary_c_context_pt ctx, int use, binary_c_string_pt v)
{
    if (v->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + v->length);
    }
    return use;
}
int binary_c_marshal_int16s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 2 * arrs->length);
    }
    return use;
}
int binary_c_marshal_int32s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 4 * arrs->length);
    }
    return use;
}
int binary_c_marshal_int64s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 8 * arrs->length);
    }
    return use;
}
int binary_c_marshal_uint16s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 2 * arrs->length);
    }
    return use;
}
int binary_c_marshal_uint32s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 4 * arrs->length);
    }
    return use;
}
int binary_c_marshal_uint64s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 8 * arrs->length);
    }
    return use;
}
int binary_c_marshal_float32s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 4 * arrs->length);
    }
    return use;
}
int binary_c_marshal_float64s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + 8 * arrs->length);
    }
    return use;
}
int binary_c_marshal_bytes_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + arrs->length);
    }
    return use;
}
int binary_c_marshal_bools_size(binary_c_context_pt ctx, int use, binary_c_bits_pt arrs)
{
    if (arrs->length > 0)
    {
        if (arrs->length > BINARY_C_MAX_UINT16)
        {
            return BINARY_C_ERROR_MAX_BUFFER;
        }
        return binary_c_marshal_size_add(ctx, use, 2 + 2 + (arrs->length + 7) / 8);
    }
    return use;
}

int binary_c_marshal_strings_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        use = binary_c_marshal_size_add(ctx, use, 2 + 2);
        if (use < 0)
        {
            return use;
        }
        else
        {
            int i = 0;
            binary_c_string_pt str;
            for (; i < arrs->length; i++)
            {
                str = &(((binary_c_string_pt)(arrs->data))[i]);
                use = binary_c_marshal_size_add(ctx, use, 2 + str->length);
                BINARY_C_CHECK_ERROR(use)
            }
        }
    }
    return use;
}
int binary_c_marshal_class_array_size(binary_c_context_pt ctx, binary_c_marshal_size_f marshal_size, binary_c_array_pt arrs, int use)
{
    if (arrs->length > 0)
    {
        int i;
        binary_c_interface_t p;
        use = binary_c_marshal_size_add(ctx, use, 4);
        BINARY_C_CHECK_ERROR(use)
        for (i = 0; i < arrs->length; i++)
        {
            use = binary_c_marshal_size_add(ctx, use, 2);
            BINARY_C_CHECK_ERROR(use)

            p = ((binary_c_interface_t *)(arrs->data))[i];
            if (p)
            {
                use = marshal_size(ctx, p, use);
                BINARY_C_CHECK_ERROR(use)
            }
        }
    }
    return use;
}
int binary_c_marshal_add(binary_c_context_pt ctx, int use, int add, int capacity)
{
    if (add > BINARY_C_MAX_UINT16)
    {
        return BINARY_C_ERROR_MAX_BUFFER;
    }
    else if (add > ctx->max_buffer || add > capacity)
    {
        return BINARY_C_ERROR_MESSAGE_TOO_LONG;
    }

    use += add;
    if (use > BINARY_C_MAX_UINT16)
    {
        return BINARY_C_ERROR_MAX_BUFFER;
    }
    else if (use > ctx->max_buffer || use > capacity)
    {
        return BINARY_C_ERROR_MESSAGE_TOO_LONG;
    }

    return use;
}
int binary_c_marshal_int16(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, int16_t v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_16)
        ctx->byte_order.put_uint16(buffer + start + 2, *(uint16_t *)(&v));
    }
    return use;
}
int binary_c_marshal_int32(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, int32_t v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 4, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_32)
        ctx->byte_order.put_uint32(buffer + start + 2, *(uint32_t *)(&v));
    }
    return use;
}
int binary_c_marshal_int64(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, int64_t v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 8, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_64)
        ctx->byte_order.put_uint64(buffer + start + 2, *(uint64_t *)(&v));
    }
    return use;
}
int binary_c_marshal_uint16(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint16_t v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_16)
        ctx->byte_order.put_uint16(buffer + start + 2, v);
    }
    return use;
}
int binary_c_marshal_uint32(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint32_t v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 4, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_32)
        ctx->byte_order.put_uint32(buffer + start + 2, v);
    }
    return use;
}
int binary_c_marshal_uint64(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint64_t v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 8, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_64)
        ctx->byte_order.put_uint64(buffer + start + 2, v);
    }
    return use;
}
int binary_c_marshal_float32(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, BINARY_C_FLOAT32_T v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 4, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_32)
        ctx->byte_order.put_uint32(buffer + start + 2, *(uint32_t *)(&v));
    }
    return use;
}
int binary_c_marshal_float64(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, BINARY_C_FLOAT64_T v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 8, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_64)
        ctx->byte_order.put_uint64(buffer + start + 2, *(uint64_t *)(&v));
    }
    return use;
}
int binary_c_marshal_byte(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint8_t v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 1, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_8)
        buffer[start + 2] = v;
    }
    return use;
}
int binary_c_marshal_bool(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, BINARY_C_BOOL v)
{
    if (v)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 2 + 1, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_8)
        buffer[start + 2] = 1;
    }
    return use;
}
int binary_c_marshal_string(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_string_pt v)
{
    if (v->length > 0)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 4 + v->length, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(v->length));
        memcpy(buffer + start + 4, v->data, v->length);
    }
    return use;
}

int binary_c_marshal_int16s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    return binary_c_marshal_uint16s(ctx, buffer, capacity, use, id, arrs);
}
int binary_c_marshal_int32s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    return binary_c_marshal_uint32s(ctx, buffer, capacity, use, id, arrs);
}
int binary_c_marshal_int64s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    return binary_c_marshal_uint64s(ctx, buffer, capacity, use, id, arrs);
}
int binary_c_marshal_uint16s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        int start = use;
        int arrs_size = arrs->length * 2;
        int i;
        use = binary_c_marshal_add(ctx, use, 4 + arrs_size, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)arrs_size);
        for (i = 0; i < arrs->length; i++)
        {
            ctx->byte_order.put_uint16(buffer + start + 4 + i * 2, ((uint16_t *)(arrs->data))[i]);
        }
    }
    return use;
}
int binary_c_marshal_uint32s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        int start = use;
        int arrs_size = arrs->length * 4;
        int i;
        use = binary_c_marshal_add(ctx, use, 4 + arrs_size, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)arrs_size);
        for (i = 0; i < arrs->length; i++)
        {
            ctx->byte_order.put_uint32(buffer + start + 4 + i * 4, ((uint32_t *)(arrs->data))[i]);
        }
    }
    return use;
}
int binary_c_marshal_uint64s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        int start = use;
        int arrs_size = arrs->length * 8;
        int i;
        use = binary_c_marshal_add(ctx, use, 4 + arrs_size, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)arrs_size);
        for (i = 0; i < arrs->length; i++)
        {
            ctx->byte_order.put_uint64(buffer + start + 4 + i * 8, ((uint64_t *)(arrs->data))[i]);
        }
    }
    return use;
}
int binary_c_marshal_float32s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    return binary_c_marshal_uint32s(ctx, buffer, capacity, use, id, arrs);
}
int binary_c_marshal_float64s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    return binary_c_marshal_uint64s(ctx, buffer, capacity, use, id, arrs);
}
int binary_c_marshal_bytes(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        int start = use;
        use = binary_c_marshal_add(ctx, use, 4 + arrs->length, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(arrs->length));
        memcpy(buffer + start + 4, arrs->data, arrs->length);
    }
    return use;
}
int binary_c_marshal_bools(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_bits_pt arrs)
{
    if (arrs->length > 0)
    {
        int start = use;
        int arrs_size = binary_c_marshal_bools_size(ctx, 0, arrs);
        BINARY_C_CHECK_ERROR(arrs_size)

        use = binary_c_marshal_add(ctx, use, arrs_size, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_BITS)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(arrs->length));
        memcpy(buffer + start + 4, arrs->data, arrs_size - 4);
    }
    return use;
}
int binary_c_marshal_strings(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs)
{
    if (arrs->length > 0)
    {
        int i;
        binary_c_string_pt str;
        int start = use;
        int arrs_size = binary_c_marshal_strings_size(ctx, 0, arrs);
        BINARY_C_CHECK_ERROR(arrs_size)

        use = binary_c_marshal_add(ctx, use, arrs_size, capacity);
        BINARY_C_CHECK_ERROR(use)

        BINARY_C_PUT_ID(ctx, buffer + start, id, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(arrs_size - 4));
        buffer += start + 4;
        for (i = 0; i < arrs->length; i++)
        {
            str = &((binary_c_string_pt)(arrs->data))[i];
            if (str->length > 0)
            {
                ctx->byte_order.put_uint16(buffer, (uint16_t)str->length);
                memcpy(buffer + 2, str->data, str->length);
                buffer += 2 + str->length;
            }
            else
            {
                ctx->byte_order.put_uint16(buffer, 0);
                buffer += 2;
            }
        }
    }
    return use;
}
int binary_c_marshal_class_array(binary_c_context_pt ctx, binary_c_marshal_f marshal, binary_c_array_pt arrs, uint8_t *buffer, int capacity, int use)
{
    binary_c_interface_t p;
    int i, start;
    for (i = 0; i < arrs->length; i++)
    {
        start = use;
        use = binary_c_marshal_add(ctx, use, 2, capacity);
        BINARY_C_CHECK_ERROR(use)

        p = ((binary_c_interface_t *)(arrs->data))[i];
        if (p)
        {
            use = marshal(ctx, p, buffer, capacity, use);
            BINARY_C_CHECK_ERROR(use)
            ctx->byte_order.put_uint16(buffer + start, (uint16_t)(use - start - 2));
        }
        else
        {
            ctx->byte_order.put_uint16(buffer + start, 0);
        }
    }
    return use;
}
int binary_c_unmarshal_field(binary_c_context_pt ctx, uint8_t *buffer, int n, binary_c_field_pt field)
{
    int ok = 0;
    if (n < 2)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD;
    }
    field->id = ctx->byte_order.uint16(buffer);
    switch (BINARY_C_GET_ID(field->id))
    {
    case BINARY_C_WRITE_8:
        field->length = 1;
        field->data = buffer + 2;
        ok = 2 + field->length;
        if (n < ok)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        break;
    case BINARY_C_WRITE_16:
        field->length = 2;
        field->data = buffer + 2;
        ok = 2 + field->length;
        if (n < ok)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        break;
    case BINARY_C_WRITE_32:
        field->length = 4;
        field->data = buffer + 2;
        ok = 2 + field->length;
        if (n < ok)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        break;
    case BINARY_C_WRITE_64:
        field->length = 8;
        field->data = buffer + 2;
        ok = 2 + field->length;
        if (n < ok)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        break;
    case BINARY_C_WRITE_LENGTH:
        if (n < 4)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD;
        }
        field->length = ctx->byte_order.uint16(buffer + 2);
        field->data = buffer + 4;
        ok = 4 + field->length;
        if (n < ok)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        break;
    case BINARY_C_WRITE_BITS:
        if (n < 4)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD;
        }
        field->length = ctx->byte_order.uint16(buffer + 2);
        field->data = buffer + 4;
        ok = 4 + (field->length + 7) / 8;
        if (n < ok)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        break;
    default:
        return BINARY_C_ERROR_UNMARSHAL_FIELD_UNKNOW_WRITE_TYPE;
    }
    field->id &= 0x1FFF;
    return ok;
}

int binary_c_unmarshal_fields(binary_c_context_pt ctx, uint8_t *buffer, int n, binary_c_field_pt fields, int count)
{
    int i = 0;
    int ok;
    binary_c_field_t field;
    while (i < count && n > 0)
    {
        ok = binary_c_unmarshal_field(ctx, buffer, n, &field);
        BINARY_C_CHECK_ERROR(ok)
        buffer += ok;
        n -= ok;
        if (n < 0)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }

        if (field.id < fields[i].id)
        {
            continue;
        }
        for (; i < count; i++)
        {
            if (field.id == fields[i].id)
            {
                fields[i].length = field.length;
                fields[i].data = field.data;
                i++;
                break;
            }
            else if (field.id < fields[i].id)
            {
                break;
            }
        }
    }
    return BINARY_C_OK;
}
int binary_c_unmarshal_count(binary_c_context_pt ctx, uint8_t *buffer, int n)
{
    int count = 0;
    int pos;
    while (n)
    {
        if (n < 2)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        pos = 2 + (int)ctx->byte_order.uint16(buffer);
        if (n < pos)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        buffer += pos;
        n -= pos;
        count++;
    }
    return count;
}
int binary_c_unmarshal_string_size(binary_c_field_pt field, int use)
{
    if (field->length > 0)
    {
        use += field->length;
    }
    return use;
}

int binary_c_unmarshal_array_size(binary_c_field_pt field, int val_size, int use)
{
    if (field->length > 0)
    {
        if (field->length % val_size != 0)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        use += field->length;
    }
    return use;
}
int binary_c_unmarshal_int16s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 2, use);
}
int binary_c_unmarshal_int32s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 4, use);
}
int binary_c_unmarshal_int64s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 8, use);
}
int binary_c_unmarshal_uint16s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 2, use);
}
int binary_c_unmarshal_uint32s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 4, use);
}
int binary_c_unmarshal_uint64s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 8, use);
}
int binary_c_unmarshal_float32s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 4, use);
}
int binary_c_unmarshal_float64s_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 8, use);
}
int binary_c_unmarshal_bytes_size(binary_c_field_pt field, int use)
{
    return binary_c_unmarshal_array_size(field, 1, use);
}
int binary_c_unmarshal_bools_size(binary_c_field_pt field, int use)
{
    if (field->length > 0)
    {
        use += (field->length + 7) / 8;
    }
    return use;
}
int binary_c_unmarshal_strings_size(binary_c_context_pt ctx, binary_c_field_pt field, int use)
{
    if (field->length > 0)
    {
        uint8_t *buffer = field->data;
        int size = field->length;

        int count = 0;
        int pos;
        int n;
        while (size > 0)
        {
            if (size < 2)
            {
                return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
            }
            n = (int)ctx->byte_order.uint16(buffer);
            pos = 2 + n;
            if (pos > size)
            {
                return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
            }
            buffer += pos;
            size -= pos;
            count++;
            use += n;
        }
        if (count > 0)
        {
            use += sizeof(binary_c_string_t) * count;
        }
    }
    return use;
}
int binary_c_unmarshal_class_array_size(binary_c_context_pt ctx, binary_c_unmarshal_size_f unmarshal_size, int class_size, binary_c_field_pt field, int use)
{
    if (field->length > 0)
    {
        uint8_t *buffer = field->data;
        int size = field->length;

        int count = 0;
        int pos;
        int n;
        while (size > 0)
        {
            if (size < 2)
            {
                return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
            }
            n = (int)ctx->byte_order.uint16(buffer);
            pos = 2 + n;
            if (pos > size)
            {
                return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
            }
            else if (n > 0)
            {
                use = unmarshal_size(ctx, buffer + 2, n, use + class_size);
                BINARY_C_CHECK_ERROR(use)
            }
            buffer += pos;
            size -= pos;
            count++;
        }
        if (count > 0)
        {
            use += sizeof(binary_c_interface_t) * count;
        }
    }
    return use;
}

int binary_c_unmarshal_bit8(binary_c_context_pt ctx, uint8_t *buffer, int n, uint8_t *output)
{
    if (n != 1)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    output[0] = buffer[0];
    return BINARY_C_OK;
}
int binary_c_unmarshal_bit16(binary_c_context_pt ctx, uint8_t *buffer, int n, uint16_t *output)
{
    if (n != 2)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    *output = ctx->byte_order.uint16(buffer);
    return BINARY_C_OK;
}
int binary_c_unmarshal_bit32(binary_c_context_pt ctx, uint8_t *buffer, int n, uint32_t *output)
{
    if (n != 4)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    *output = ctx->byte_order.uint32(buffer);
    return BINARY_C_OK;
}
int binary_c_unmarshal_bit64(binary_c_context_pt ctx, uint8_t *buffer, int n, uint64_t *output)
{
    if (n != 8)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    *output = ctx->byte_order.uint64(buffer);
    return BINARY_C_OK;
}
int binary_c_unmarshal_array_bit(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_array_pt arrs, int valSeize)
{
    if (n < 1 || n % valSeize)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    if (valSeize == 1)
    {
        binary_c_interface_t p = binary_c_malloc(allocator, n);
        if (!p)
        {
            return BINARY_C_ERROR_MALLOC;
        }
        memcpy(p, buffer, n);
        arrs->capacity = n;
        arrs->length = n;
        arrs->data = p;
    }
    else
    {
        if (valSeize < 2 || valSeize > 8 || valSeize % 2 != 0)
        {
            return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
        }
        binary_c_interface_t p = binary_c_malloc(allocator, n);
        if (p)
        {
            int count = n / valSeize;
            int i = 0;
            for (; i < count; i++)
            {
                switch (valSeize)
                {
                case 2:
                    ((uint16_t *)p)[i] = ctx->byte_order.uint16(buffer + i * valSeize);
                    break;
                case 4:
                    ((uint32_t *)p)[i] = ctx->byte_order.uint32(buffer + i * valSeize);
                    break;
                case 8:
                    ((uint64_t *)p)[i] = ctx->byte_order.uint64(buffer + i * valSeize);
                    break;
                }
            }
            arrs->capacity = count;
            arrs->length = count;
            arrs->data = p;
        }
        else
        {
            return BINARY_C_ERROR_MALLOC;
        }
    }
    return BINARY_C_OK;
}
int binary_c_unmarshal_bits(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_bits_pt bits)
{
    if (n < 1)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    else
    {
        int size = (n + 7) / 8;
        uint8_t *p = binary_c_malloc(allocator, size);
        if (!p)
        {
            return BINARY_C_ERROR_MALLOC;
        }
        memcpy(p, buffer, size);
        bits->capacity = size * 8;
        bits->length = n;
        bits->data = p;
    }
    return BINARY_C_OK;
}
int binary_c_unmarshal_string(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_string_pt str)
{
    if (n < 1)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    else
    {
        char *p = binary_c_malloc(allocator, n);
        if (!p)
        {
            return BINARY_C_ERROR_MALLOC;
        }
        memcpy(p, buffer, n);
        str->capacity = n;
        str->length = n;
        str->data = p;
    }
    return BINARY_C_OK;
}
int binary_c_unmarshal_strings(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_array_pt strs)
{
    if (n < 1)
    {
        return BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH;
    }
    else
    {
        int count = binary_c_unmarshal_count(ctx, buffer, n);
        if (count < 0)
        {
            return count;
        }
        else
        {
            int length = allocator->buffer.length;
            binary_c_string_pt p = binary_c_malloc(allocator, sizeof(binary_c_string_t) * count);
            if (p)
            {
                int i = 0;
                binary_c_string_pt str;
                for (; i < count; i++)
                {
                    str = p + i;
                    str->length = ctx->byte_order.uint16(buffer);
                    str->capacity = str->length;
                    if (str->length)
                    {
                        str->data = binary_c_malloc(allocator, str->length);
                        if (!str->data)
                        {
                            if (i > 0)
                            {
                                for (i--; i >= 0; i--)
                                {
                                    str = p + i;
                                    binary_c_free(allocator, str->data, length);
                                }
                            }
                            binary_c_free(allocator, p, length);
                            return BINARY_C_ERROR_MALLOC;
                        }
                        memcpy(str->data, buffer + 2, str->length);
                        buffer += 2 + str->length;
                    }
                    else
                    {
                        str->data = NULL;
                        buffer += 2;
                    }
                }
                strs->capacity = count;
                strs->length = count;
                strs->data = p;
            }
            else
            {
                return BINARY_C_ERROR_MALLOC;
            }
        }
    }
    return BINARY_C_OK;
}
int binary_c_unmarshal_class(binary_c_unmarshal_class_f unmarshal_class, binary_c_context_pt ctx, binary_c_allocator_pt allocator, binary_c_field_pt field, binary_c_interface_t *p, size_t size)
{
    int ok = BINARY_C_OK;
    int length = allocator->buffer.length;
    binary_c_interface_t ptr = binary_c_malloc(allocator, size);
    if (ptr)
    {
        ok = unmarshal_class(ctx, allocator, field->data, field->length, ptr);
        if (ok < 0)
        {
            binary_c_free(allocator, ptr, length);
            return ok;
        }
        *p = ptr;
    }
    else
    {
        ok = BINARY_C_ERROR_MALLOC;
    }
    return ok;
}
int binary_c_unmarshal_class_array(binary_c_unmarshal_class_f unmarshal_class, binary_c_free_class_f free_class, binary_c_context_pt ctx, binary_c_allocator_pt allocator, binary_c_field_pt field, binary_c_array_pt arrs, size_t size)
{
    int ok = BINARY_C_OK;
    int count = binary_c_unmarshal_count(ctx, field->data, field->length);
    if (count < 0)
    {
        return count;
    }
    else
    {
        int length = allocator->buffer.length;
        binary_c_interface_t p = binary_c_malloc(allocator, sizeof(uint8_t *) * count);
        if (p)
        {
            int i = 0;
            uint16_t class_size;
            uint8_t *buffer = field->data;
            for (; i < count; i++)
            {
                class_size = ctx->byte_order.uint16(buffer);
                if (class_size)
                {
                    binary_c_interface_t item = binary_c_malloc(allocator, size);
                    if (!item)
                    {
                        if (i > 0)
                        {
                            binary_c_free_f free = NULL;
                            if (allocator->malloc)
                            {
                                free = allocator->free;
                            }
                            for (i--; i >= 0; i--)
                            {
                                free_class(free, ((binary_c_interface_t *)p)[i]);
                            }
                        }
                        binary_c_free(allocator, p, length);
                        return BINARY_C_ERROR_MALLOC;
                    }
                    ok = unmarshal_class(ctx, allocator, buffer + 2, class_size, item);
                    if (BINARY_C_HAS_ERROR(ok))
                    {
                        if (i > 0)
                        {
                            binary_c_free_f free = NULL;
                            if (allocator->malloc)
                            {
                                free = allocator->free;
                            }
                            for (i--; i >= 0; i--)
                            {
                                free_class(free, ((binary_c_interface_t *)p)[i]);
                            }
                        }
                        binary_c_free(allocator, p, length);
                        return ok;
                    }

                    ((binary_c_interface_t *)p)[i] = item;
                    buffer += 2 + class_size;
                }
                else
                {
                    ((binary_c_interface_t *)p)[i] = NULL;
                    buffer += 2;
                }
            }
            arrs->capacity = count;
            arrs->length = count;
            arrs->data = p;
        }
        else
        {
            ok = BINARY_C_ERROR_MALLOC;
        }
    }
    return ok;
}