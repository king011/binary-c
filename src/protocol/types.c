#include "types.h"

void binary_c_types_types_reset(binary_c_types_types_pt msg)
{
	if (msg)
	{
		memset(msg, 0, sizeof(binary_c_types_types_t));
	}
}
void binary_c_types_types_free(binary_c_free_f free, binary_c_types_types_pt msg)
{
	if (msg)
	{
		if (free)
		{
			binary_c_free_string(free, &(msg->str));
			if (msg->cat)
			{
				binary_c_zoo_animal_cat_free(free, msg->cat);
				free(msg->cat);
			}
			if (msg->dog)
			{
				binary_c_zoo_animal_dog_free(free, msg->dog);
				free(msg->dog);
			}
			binary_c_free_array(free, &(msg->int16s));
			binary_c_free_array(free, &(msg->int32s));
			binary_c_free_array(free, &(msg->int64s));
			binary_c_free_array(free, &(msg->uint16s));
			binary_c_free_array(free, &(msg->uint32s));
			binary_c_free_array(free, &(msg->uint64s));
			binary_c_free_array(free, &(msg->float32s));
			binary_c_free_array(free, &(msg->float64s));
			binary_c_free_array(free, &(msg->bs));
			binary_c_free_bits(free, &(msg->oks));
			binary_c_free_strings(free, &(msg->strs));
			binary_c_free_class_array((binary_c_free_class_f)binary_c_zoo_animal_cat_free, free, &(msg->cats));
			binary_c_free_class_array((binary_c_free_class_f)binary_c_zoo_animal_dog_free, free, &(msg->dogs));
			binary_c_free_array(free, &(msg->poss));
		}
		memset(msg, 0, sizeof(binary_c_types_types_t));
	}
}
int binary_c_types_types_marshal_size(binary_c_context_pt ctx, binary_c_types_types_pt msg, int use)
{
	if (!msg)
	{
		return use;
	}
	use = binary_c_marshal_int16_size(ctx, use, msg->int16);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int32_size(ctx, use, msg->int32);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int64_size(ctx, use, msg->int64);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint16_size(ctx, use, msg->uint16);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint32_size(ctx, use, msg->uint32);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint64_size(ctx, use, msg->uint64);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float32_size(ctx, use, msg->float32);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float64_size(ctx, use, msg->float64);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_byte_size(ctx, use, msg->b);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_bool_size(ctx, use, msg->ok);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_string_size(ctx, use, &(msg->str));
	BINARY_C_CHECK_ERROR(use)
	if (msg->cat)
	{
		use = binary_c_marshal_size_add(ctx, use, 2 + 2);
		BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_cat_marshal_size(ctx, msg->cat, use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (msg->dog)
	{
		use = binary_c_marshal_size_add(ctx, use, 2 + 2);
		BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_dog_marshal_size(ctx, msg->dog, use);
		BINARY_C_CHECK_ERROR(use)
	}
	use = binary_c_marshal_int16_size(ctx, use, msg->pos);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int16s_size(ctx, use, &(msg->int16s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int32s_size(ctx, use, &(msg->int32s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int64s_size(ctx, use, &(msg->int64s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint16s_size(ctx, use, &(msg->uint16s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint32s_size(ctx, use, &(msg->uint32s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint64s_size(ctx, use, &(msg->uint64s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float32s_size(ctx, use, &(msg->float32s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float64s_size(ctx, use, &(msg->float64s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_bytes_size(ctx, use, &(msg->bs));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_bools_size(ctx, use, &(msg->oks));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_strings_size(ctx, use, &(msg->strs));
	BINARY_C_CHECK_ERROR(use)
	if (msg->cats.length)
	{
		use = binary_c_marshal_class_array_size(ctx, (binary_c_marshal_size_f)binary_c_zoo_animal_cat_marshal_size, &(msg->cats), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (msg->dogs.length)
	{
		use = binary_c_marshal_class_array_size(ctx, (binary_c_marshal_size_f)binary_c_zoo_animal_dog_marshal_size, &(msg->dogs), use);
		BINARY_C_CHECK_ERROR(use)
	}
	use = binary_c_marshal_int16s_size(ctx, use, &(msg->poss));
	BINARY_C_CHECK_ERROR(use)

	return use;
}
int binary_c_types_types_marshal(binary_c_context_pt ctx, binary_c_types_types_pt msg, uint8_t *buffer, int capacity, int use)
{
	if(!msg)
	{
		return use;
	}
	use = binary_c_marshal_int16(ctx, buffer, capacity, use, 1, msg->int16);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int32(ctx, buffer, capacity, use, 2, msg->int32);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int64(ctx, buffer, capacity, use, 3, msg->int64);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint16(ctx, buffer, capacity, use, 4, msg->uint16);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint32(ctx, buffer, capacity, use, 5, msg->uint32);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint64(ctx, buffer, capacity, use, 6, msg->uint64);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float32(ctx, buffer, capacity, use, 7, msg->float32);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float64(ctx, buffer, capacity, use, 8, msg->float64);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_byte(ctx, buffer, capacity, use, 9, msg->b);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_bool(ctx, buffer, capacity, use, 10, msg->ok);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_string(ctx, buffer, capacity, use, 11, &(msg->str));
	BINARY_C_CHECK_ERROR(use)
	if (msg->cat)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_cat_marshal(ctx, msg->cat, buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, 12, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
	if (msg->dog)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_dog_marshal(ctx, msg->dog, buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, 13, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
	use = binary_c_marshal_int16(ctx, buffer, capacity, use, 14, msg->pos);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int16s(ctx, buffer, capacity, use, 21, &(msg->int16s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int32s(ctx, buffer, capacity, use, 22, &(msg->int32s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_int64s(ctx, buffer, capacity, use, 23, &(msg->int64s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint16s(ctx, buffer, capacity, use, 24, &(msg->uint16s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint32s(ctx, buffer, capacity, use, 25, &(msg->uint32s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_uint64s(ctx, buffer, capacity, use, 26, &(msg->uint64s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float32s(ctx, buffer, capacity, use, 27, &(msg->float32s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_float64s(ctx, buffer, capacity, use, 28, &(msg->float64s));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_bytes(ctx, buffer, capacity, use, 29, &(msg->bs));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_bools(ctx, buffer, capacity, use, 30, &(msg->oks));
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_strings(ctx, buffer, capacity, use, 31, &(msg->strs));
	BINARY_C_CHECK_ERROR(use)
	if (msg->cats.length > 0)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_marshal_class_array(ctx, (binary_c_marshal_f)binary_c_zoo_animal_cat_marshal, &(msg->cats), buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, 32, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
	if (msg->dogs.length > 0)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_marshal_class_array(ctx, (binary_c_marshal_f)binary_c_zoo_animal_dog_marshal, &(msg->dogs), buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, 33, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
	use = binary_c_marshal_int16s(ctx, buffer, capacity, use, 34, &(msg->poss));
	BINARY_C_CHECK_ERROR(use)
	return use;
}
int binary_c_types_types_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use)
{
	int ok = BINARY_C_OK;
	binary_c_field_t fields[17] = {0};
	fields[0].id = 11;
	fields[1].id = 12;
	fields[2].id = 13;
	fields[3].id = 21;
	fields[4].id = 22;
	fields[5].id = 23;
	fields[6].id = 24;
	fields[7].id = 25;
	fields[8].id = 26;
	fields[9].id = 27;
	fields[10].id = 28;
	fields[11].id = 29;
	fields[12].id = 30;
	fields[13].id = 31;
	fields[14].id = 32;
	fields[15].id = 33;
	fields[16].id = 34;
	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, 17);
	BINARY_C_CHECK_ERROR(ok)
	if (fields[0].length)
	{
		use = binary_c_unmarshal_string_size(&(fields[0]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[1].length)
	{
		use = binary_c_zoo_animal_cat_unmarshal_size(ctx, fields[1].data, fields[1].length, use + sizeof(binary_c_zoo_animal_cat_t));
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[2].length)
	{
		use = binary_c_zoo_animal_dog_unmarshal_size(ctx, fields[2].data, fields[2].length, use + sizeof(binary_c_zoo_animal_dog_t));
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[3].length)
	{
		use = binary_c_unmarshal_int16s_size(&(fields[3]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[4].length)
	{
		use = binary_c_unmarshal_int32s_size(&(fields[4]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[5].length)
	{
		use = binary_c_unmarshal_int64s_size(&(fields[5]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[6].length)
	{
		use = binary_c_unmarshal_uint16s_size(&(fields[6]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[7].length)
	{
		use = binary_c_unmarshal_uint32s_size(&(fields[7]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[8].length)
	{
		use = binary_c_unmarshal_uint64s_size(&(fields[8]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[9].length)
	{
		use = binary_c_unmarshal_float32s_size(&(fields[9]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[10].length)
	{
		use = binary_c_unmarshal_float64s_size(&(fields[10]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[11].length)
	{
		use = binary_c_unmarshal_bytes_size(&(fields[11]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[12].length)
	{
		use = binary_c_unmarshal_bools_size(&(fields[12]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[13].length)
	{
		use = binary_c_unmarshal_strings_size(ctx, &(fields[13]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[14].length)
	{
		use = binary_c_unmarshal_class_array_size(ctx, (binary_c_unmarshal_size_f)binary_c_zoo_animal_cat_unmarshal_size, sizeof(binary_c_zoo_animal_cat_t), &(fields[14]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[15].length)
	{
		use = binary_c_unmarshal_class_array_size(ctx, (binary_c_unmarshal_size_f)binary_c_zoo_animal_dog_unmarshal_size, sizeof(binary_c_zoo_animal_dog_t), &(fields[15]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[16].length)
	{
		use = binary_c_unmarshal_int16s_size(&(fields[16]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	return use;
}
int binary_c_types_types_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_types_types_pt msg)
{
	int ok = BINARY_C_OK;
	int length = allocator->buffer.length;
	binary_c_field_t fields[28];
	memset(fields, 0, sizeof(binary_c_field_t)*28);
	binary_c_types_types_reset(msg);
	fields[0].id = 1;
	fields[1].id = 2;
	fields[2].id = 3;
	fields[3].id = 4;
	fields[4].id = 5;
	fields[5].id = 6;
	fields[6].id = 7;
	fields[7].id = 8;
	fields[8].id = 9;
	fields[9].id = 10;
	fields[10].id = 11;
	fields[11].id = 12;
	fields[12].id = 13;
	fields[13].id = 14;
	fields[14].id = 21;
	fields[15].id = 22;
	fields[16].id = 23;
	fields[17].id = 24;
	fields[18].id = 25;
	fields[19].id = 26;
	fields[20].id = 27;
	fields[21].id = 28;
	fields[22].id = 29;
	fields[23].id = 30;
	fields[24].id = 31;
	fields[25].id = 32;
	fields[26].id = 33;
	fields[27].id = 34;
	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, 28);
	BINARY_C_CHECK_ERROR(ok)
	if (fields[0].length)
	{
		ok = binary_c_unmarshal_bit16(ctx, fields[0].data, fields[0].length, (uint16_t *)&(msg->int16));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[1].length)
	{
		ok = binary_c_unmarshal_bit32(ctx, fields[1].data, fields[1].length, (uint32_t *)&(msg->int32));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[2].length)
	{
		ok = binary_c_unmarshal_bit64(ctx, fields[2].data, fields[2].length, (uint64_t *)&(msg->int64));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[3].length)
	{
		ok = binary_c_unmarshal_bit16(ctx, fields[3].data, fields[3].length, (uint16_t *)&(msg->uint16));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[4].length)
	{
		ok = binary_c_unmarshal_bit32(ctx, fields[4].data, fields[4].length, (uint32_t *)&(msg->uint32));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[5].length)
	{
		ok = binary_c_unmarshal_bit64(ctx, fields[5].data, fields[5].length, (uint64_t *)&(msg->uint64));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[6].length)
	{
		ok = binary_c_unmarshal_bit32(ctx, fields[6].data, fields[6].length, (uint32_t *)&(msg->float32));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[7].length)
	{
		ok = binary_c_unmarshal_bit64(ctx, fields[7].data, fields[7].length, (uint64_t *)&(msg->float64));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[8].length)
	{
		ok = binary_c_unmarshal_bit8(ctx, fields[8].data, fields[8].length, (uint8_t *)&(msg->b));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[9].length)
	{
		ok = binary_c_unmarshal_bit8(ctx, fields[9].data, fields[9].length, (uint8_t *)&(msg->ok));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[10].length)
	{
		ok = binary_c_unmarshal_string(ctx, allocator, fields[10].data, fields[10].length, &(msg->str));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[11].length)
	{
		ok = binary_c_unmarshal_class((binary_c_unmarshal_class_f)binary_c_zoo_animal_cat_unmarshal, ctx, allocator, &fields[11], &(msg->cat), sizeof(binary_c_zoo_animal_cat_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[12].length)
	{
		ok = binary_c_unmarshal_class((binary_c_unmarshal_class_f)binary_c_zoo_animal_dog_unmarshal, ctx, allocator, &fields[12], &(msg->dog), sizeof(binary_c_zoo_animal_dog_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[13].length)
	{
		ok = binary_c_unmarshal_bit16(ctx, fields[13].data, fields[13].length, (uint16_t *)&(msg->pos));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[14].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[14].data, fields[14].length, &(msg->int16s), 2);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[15].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[15].data, fields[15].length, &(msg->int32s), 4);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[16].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[16].data, fields[16].length, &(msg->int64s), 8);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[17].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[17].data, fields[17].length, &(msg->uint16s), 2);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[18].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[18].data, fields[18].length, &(msg->uint32s), 4);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[19].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[19].data, fields[19].length, &(msg->uint64s), 8);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[20].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[20].data, fields[20].length, &(msg->float32s), 4);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[21].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[21].data, fields[21].length, &(msg->float64s), 8);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[22].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[22].data, fields[22].length, &(msg->bs), 1);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[23].length)
	{
		ok = binary_c_unmarshal_bits(ctx, allocator, fields[23].data, fields[23].length, &(msg->oks));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[24].length)
	{
		ok = binary_c_unmarshal_strings(ctx, allocator, fields[24].data, fields[24].length, &(msg->strs));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[25].length)
	{
		ok = binary_c_unmarshal_class_array((binary_c_unmarshal_class_f)binary_c_zoo_animal_cat_unmarshal, (binary_c_free_class_f)binary_c_zoo_animal_cat_free, ctx, allocator, &fields[25], &(msg->cats), sizeof(binary_c_zoo_animal_cat_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[26].length)
	{
		ok = binary_c_unmarshal_class_array((binary_c_unmarshal_class_f)binary_c_zoo_animal_dog_unmarshal, (binary_c_free_class_f)binary_c_zoo_animal_dog_free, ctx, allocator, &fields[26], &(msg->dogs), sizeof(binary_c_zoo_animal_dog_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	if (fields[27].length)
	{
		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[27].data, fields[27].length, &(msg->poss), 2);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_types_types_free, allocator, length, msg)
	}
	return ok;
}

void binary_c_types_types_cat_set(binary_c_types_types_pt msg,binary_c_zoo_animal_cat_pt val)
{
	msg->cat = val;
}
binary_c_zoo_animal_cat_pt binary_c_types_types_cat_get(binary_c_types_types_pt msg)
{
	return (binary_c_zoo_animal_cat_pt)(msg->cat);
}
void binary_c_types_types_dog_set(binary_c_types_types_pt msg,binary_c_zoo_animal_dog_pt val)
{
	msg->dog = val;
}
binary_c_zoo_animal_dog_pt binary_c_types_types_dog_get(binary_c_types_types_pt msg)
{
	return (binary_c_zoo_animal_dog_pt)(msg->dog);
}
void binary_c_types_types_int16s_set(binary_c_types_types_pt msg,int i,int16_t val)
{
	((int16_t*)(msg->int16s.data))[i] = val;
}
int16_t binary_c_types_types_int16s_get(binary_c_types_types_pt msg,int i)
{
	return ((int16_t*)(msg->int16s.data))[i];
}
void binary_c_types_types_int32s_set(binary_c_types_types_pt msg,int i,int32_t val)
{
	((int32_t*)(msg->int32s.data))[i] = val;
}
int32_t binary_c_types_types_int32s_get(binary_c_types_types_pt msg,int i)
{
	return ((int32_t*)(msg->int32s.data))[i];
}
void binary_c_types_types_int64s_set(binary_c_types_types_pt msg,int i,int64_t val)
{
	((int64_t*)(msg->int64s.data))[i] = val;
}
int64_t binary_c_types_types_int64s_get(binary_c_types_types_pt msg,int i)
{
	return ((int64_t*)(msg->int64s.data))[i];
}
void binary_c_types_types_uint16s_set(binary_c_types_types_pt msg,int i,uint16_t val)
{
	((uint16_t*)(msg->uint16s.data))[i] = val;
}
uint16_t binary_c_types_types_uint16s_get(binary_c_types_types_pt msg,int i)
{
	return ((uint16_t*)(msg->uint16s.data))[i];
}
void binary_c_types_types_uint32s_set(binary_c_types_types_pt msg,int i,uint32_t val)
{
	((uint32_t*)(msg->uint32s.data))[i] = val;
}
uint32_t binary_c_types_types_uint32s_get(binary_c_types_types_pt msg,int i)
{
	return ((uint32_t*)(msg->uint32s.data))[i];
}
void binary_c_types_types_uint64s_set(binary_c_types_types_pt msg,int i,uint64_t val)
{
	((uint64_t*)(msg->uint64s.data))[i] = val;
}
uint64_t binary_c_types_types_uint64s_get(binary_c_types_types_pt msg,int i)
{
	return ((uint64_t*)(msg->uint64s.data))[i];
}
void binary_c_types_types_float32s_set(binary_c_types_types_pt msg,int i,BINARY_C_FLOAT32_T val)
{
	((BINARY_C_FLOAT32_T*)(msg->float32s.data))[i] = val;
}
BINARY_C_FLOAT32_T binary_c_types_types_float32s_get(binary_c_types_types_pt msg,int i)
{
	return ((BINARY_C_FLOAT32_T*)(msg->float32s.data))[i];
}
void binary_c_types_types_float64s_set(binary_c_types_types_pt msg,int i,BINARY_C_FLOAT64_T val)
{
	((BINARY_C_FLOAT64_T*)(msg->float64s.data))[i] = val;
}
BINARY_C_FLOAT64_T binary_c_types_types_float64s_get(binary_c_types_types_pt msg,int i)
{
	return ((BINARY_C_FLOAT64_T*)(msg->float64s.data))[i];
}
void binary_c_types_types_bs_set(binary_c_types_types_pt msg,int i,uint8_t val)
{
	((uint8_t*)(msg->bs.data))[i] = val;
}
uint8_t binary_c_types_types_bs_get(binary_c_types_types_pt msg,int i)
{
	return ((uint8_t*)(msg->bs.data))[i];
}
int binary_c_types_types_oks_set(binary_c_types_types_pt msg,int i,BINARY_C_BOOL val)
{
	return binary_c_bits_set(&(msg->oks),i,val);
}
BINARY_C_BOOL binary_c_types_types_oks_get(binary_c_types_types_pt msg,int i)
{
	return binary_c_bits_get(&(msg->oks),i);
}
void binary_c_types_types_strs_set(binary_c_types_types_pt msg,int i,binary_c_string_t val)
{
	((binary_c_string_t*)(msg->strs.data))[i] = val;
}
binary_c_string_t binary_c_types_types_strs_get(binary_c_types_types_pt msg,int i)
{
	return ((binary_c_string_t*)(msg->strs.data))[i];
}
void binary_c_types_types_cats_set(binary_c_types_types_pt msg,int i,binary_c_zoo_animal_cat_pt val)
{
	((binary_c_zoo_animal_cat_pt*)(msg->cats.data))[i] = val;
}
binary_c_zoo_animal_cat_pt binary_c_types_types_cats_get(binary_c_types_types_pt msg,int i)
{
	return ((binary_c_zoo_animal_cat_pt*)(msg->cats.data))[i];
}
void binary_c_types_types_dogs_set(binary_c_types_types_pt msg,int i,binary_c_zoo_animal_dog_pt val)
{
	((binary_c_zoo_animal_dog_pt*)(msg->dogs.data))[i] = val;
}
binary_c_zoo_animal_dog_pt binary_c_types_types_dogs_get(binary_c_types_types_pt msg,int i)
{
	return ((binary_c_zoo_animal_dog_pt*)(msg->dogs.data))[i];
}
void binary_c_types_types_poss_set(binary_c_types_types_pt msg,int i,int16_t val)
{
	((int16_t*)(msg->poss.data))[i] = val;
}
int16_t binary_c_types_types_poss_get(binary_c_types_types_pt msg,int i)
{
	return ((int16_t*)(msg->poss.data))[i];
}

