#include "animal.h"

const char *binary_c_zoo_animal_pos_enum_string(uint16_t val)
{
	const char *str;
	switch (val)
	{
	case 0:
		str = "pool";
		break;
	case 10:
		str = "cage";
		break;
	case 11:
		str = "step";
		break;
	case 12:
		str = "stairs";
		break;
	case 13:
		str = "corridor p0";
		break;
	case 14:
		str = "corridor p1";
		break;
	default:
		str = "unknow";
		break;
	}
	return str;
}
uint16_t binary_c_zoo_animal_pos_enum_val(const char *str)
{
	uint16_t val = 0;
	if (!strcmp(str, "pool"))
	{
		val = 0;
	}
	else if (!strcmp(str, "cage"))
	{
		val = 10;
	}
	else if (!strcmp(str, "step"))
	{
		val = 11;
	}
	else if (!strcmp(str, "stairs"))
	{
		val = 12;
	}
	else if (!strcmp(str, "corridor p0"))
	{
		val = 13;
	}
	else if (!strcmp(str, "corridor p1"))
	{
		val = 14;
	}
	
	return val;
}
void binary_c_zoo_animal_cat_reset(binary_c_zoo_animal_cat_pt msg)
{
	if (msg)
	{
		memset(msg, 0, sizeof(binary_c_zoo_animal_cat_t));
	}
}
void binary_c_zoo_animal_cat_free(binary_c_free_f free, binary_c_zoo_animal_cat_pt msg)
{
	if (msg)
	{
		if (free)
		{
			binary_c_free_string(free, &(msg->name));
			if (msg->dog)
			{
				binary_c_zoo_animal_dog_free(free, msg->dog);
				free(msg->dog);
			}
			if (msg->cat)
			{
				binary_c_zoo_animal_cat_free(free, msg->cat);
				free(msg->cat);
			}
		}
		memset(msg, 0, sizeof(binary_c_zoo_animal_cat_t));
	}
}
int binary_c_zoo_animal_cat_marshal_size(binary_c_context_pt ctx, binary_c_zoo_animal_cat_pt msg, int use)
{
	if (!msg)
	{
		return use;
	}
	use = binary_c_marshal_string_size(ctx, use, &(msg->name));
	BINARY_C_CHECK_ERROR(use)
	if (msg->dog)
	{
		use = binary_c_marshal_size_add(ctx, use, 2 + 2);
		BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_dog_marshal_size(ctx, msg->dog, use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (msg->cat)
	{
		use = binary_c_marshal_size_add(ctx, use, 2 + 2);
		BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_cat_marshal_size(ctx, msg->cat, use);
		BINARY_C_CHECK_ERROR(use)
	}

	return use;
}
int binary_c_zoo_animal_cat_marshal(binary_c_context_pt ctx, binary_c_zoo_animal_cat_pt msg, uint8_t *buffer, int capacity, int use)
{
	if(!msg)
	{
		return use;
	}
	use = binary_c_marshal_string(ctx, buffer, capacity, use, 1, &(msg->name));
	BINARY_C_CHECK_ERROR(use)
	if (msg->dog)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_dog_marshal(ctx, msg->dog, buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, 2, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
	if (msg->cat)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_cat_marshal(ctx, msg->cat, buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, 3, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
	return use;
}
int binary_c_zoo_animal_cat_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use)
{
	int ok = BINARY_C_OK;
	binary_c_field_t fields[3] = {0};
	fields[0].id = 1;
	fields[1].id = 2;
	fields[2].id = 3;
	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, 3);
	BINARY_C_CHECK_ERROR(ok)
	if (fields[0].length)
	{
		use = binary_c_unmarshal_string_size(&(fields[0]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[1].length)
	{
		use = binary_c_zoo_animal_dog_unmarshal_size(ctx, fields[1].data, fields[1].length, use + sizeof(binary_c_zoo_animal_dog_t));
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[2].length)
	{
		use = binary_c_zoo_animal_cat_unmarshal_size(ctx, fields[2].data, fields[2].length, use + sizeof(binary_c_zoo_animal_cat_t));
		BINARY_C_CHECK_ERROR(use)
	}
	return use;
}
int binary_c_zoo_animal_cat_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_zoo_animal_cat_pt msg)
{
	int ok = BINARY_C_OK;
	int length = allocator->buffer.length;
	binary_c_field_t fields[3];
	memset(fields, 0, sizeof(binary_c_field_t)*3);
	binary_c_zoo_animal_cat_reset(msg);
	fields[0].id = 1;
	fields[1].id = 2;
	fields[2].id = 3;
	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, 3);
	BINARY_C_CHECK_ERROR(ok)
	if (fields[0].length)
	{
		ok = binary_c_unmarshal_string(ctx, allocator, fields[0].data, fields[0].length, &(msg->name));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_zoo_animal_cat_free, allocator, length, msg)
	}
	if (fields[1].length)
	{
		ok = binary_c_unmarshal_class((binary_c_unmarshal_class_f)binary_c_zoo_animal_dog_unmarshal, ctx, allocator, &fields[1], &(msg->dog), sizeof(binary_c_zoo_animal_dog_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_zoo_animal_cat_free, allocator, length, msg)
	}
	if (fields[2].length)
	{
		ok = binary_c_unmarshal_class((binary_c_unmarshal_class_f)binary_c_zoo_animal_cat_unmarshal, ctx, allocator, &fields[2], &(msg->cat), sizeof(binary_c_zoo_animal_cat_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_zoo_animal_cat_free, allocator, length, msg)
	}
	return ok;
}

void binary_c_zoo_animal_cat_dog_set(binary_c_zoo_animal_cat_pt msg,binary_c_zoo_animal_dog_pt val)
{
	msg->dog = val;
}
binary_c_zoo_animal_dog_pt binary_c_zoo_animal_cat_dog_get(binary_c_zoo_animal_cat_pt msg)
{
	return (binary_c_zoo_animal_dog_pt)(msg->dog);
}
void binary_c_zoo_animal_cat_cat_set(binary_c_zoo_animal_cat_pt msg,binary_c_zoo_animal_cat_pt val)
{
	msg->cat = val;
}
binary_c_zoo_animal_cat_pt binary_c_zoo_animal_cat_cat_get(binary_c_zoo_animal_cat_pt msg)
{
	return (binary_c_zoo_animal_cat_pt)(msg->cat);
}

void binary_c_zoo_animal_dog_reset(binary_c_zoo_animal_dog_pt msg)
{
	if (msg)
	{
		memset(msg, 0, sizeof(binary_c_zoo_animal_dog_t));
	}
}
void binary_c_zoo_animal_dog_free(binary_c_free_f free, binary_c_zoo_animal_dog_pt msg)
{
	if (msg)
	{
		if (free)
		{
			binary_c_free_strings(free, &(msg->eat));
			if (msg->cat)
			{
				binary_c_zoo_animal_cat_free(free, msg->cat);
				free(msg->cat);
			}
		}
		memset(msg, 0, sizeof(binary_c_zoo_animal_dog_t));
	}
}
int binary_c_zoo_animal_dog_marshal_size(binary_c_context_pt ctx, binary_c_zoo_animal_dog_pt msg, int use)
{
	if (!msg)
	{
		return use;
	}
	use = binary_c_marshal_int64_size(ctx, use, msg->id);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_strings_size(ctx, use, &(msg->eat));
	BINARY_C_CHECK_ERROR(use)
	if (msg->cat)
	{
		use = binary_c_marshal_size_add(ctx, use, 2 + 2);
		BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_cat_marshal_size(ctx, msg->cat, use);
		BINARY_C_CHECK_ERROR(use)
	}

	return use;
}
int binary_c_zoo_animal_dog_marshal(binary_c_context_pt ctx, binary_c_zoo_animal_dog_pt msg, uint8_t *buffer, int capacity, int use)
{
	if(!msg)
	{
		return use;
	}
	use = binary_c_marshal_int64(ctx, buffer, capacity, use, 1, msg->id);
	BINARY_C_CHECK_ERROR(use)
	use = binary_c_marshal_strings(ctx, buffer, capacity, use, 2, &(msg->eat));
	BINARY_C_CHECK_ERROR(use)
	if (msg->cat)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_zoo_animal_cat_marshal(ctx, msg->cat, buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, 3, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
	return use;
}
int binary_c_zoo_animal_dog_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use)
{
	int ok = BINARY_C_OK;
	binary_c_field_t fields[2] = {0};
	fields[0].id = 2;
	fields[1].id = 3;
	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, 2);
	BINARY_C_CHECK_ERROR(ok)
	if (fields[0].length)
	{
		use = binary_c_unmarshal_strings_size(ctx, &(fields[0]), use);
		BINARY_C_CHECK_ERROR(use)
	}
	if (fields[1].length)
	{
		use = binary_c_zoo_animal_cat_unmarshal_size(ctx, fields[1].data, fields[1].length, use + sizeof(binary_c_zoo_animal_cat_t));
		BINARY_C_CHECK_ERROR(use)
	}
	return use;
}
int binary_c_zoo_animal_dog_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_zoo_animal_dog_pt msg)
{
	int ok = BINARY_C_OK;
	int length = allocator->buffer.length;
	binary_c_field_t fields[3];
	memset(fields, 0, sizeof(binary_c_field_t)*3);
	binary_c_zoo_animal_dog_reset(msg);
	fields[0].id = 1;
	fields[1].id = 2;
	fields[2].id = 3;
	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, 3);
	BINARY_C_CHECK_ERROR(ok)
	if (fields[0].length)
	{
		ok = binary_c_unmarshal_bit64(ctx, fields[0].data, fields[0].length, (uint64_t *)&(msg->id));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_zoo_animal_dog_free, allocator, length, msg)
	}
	if (fields[1].length)
	{
		ok = binary_c_unmarshal_strings(ctx, allocator, fields[1].data, fields[1].length, &(msg->eat));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_zoo_animal_dog_free, allocator, length, msg)
	}
	if (fields[2].length)
	{
		ok = binary_c_unmarshal_class((binary_c_unmarshal_class_f)binary_c_zoo_animal_cat_unmarshal, ctx, allocator, &fields[2], &(msg->cat), sizeof(binary_c_zoo_animal_cat_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_zoo_animal_dog_free, allocator, length, msg)
	}
	return ok;
}

void binary_c_zoo_animal_dog_eat_set(binary_c_zoo_animal_dog_pt msg,int i,binary_c_string_t val)
{
	((binary_c_string_t*)(msg->eat.data))[i] = val;
}
binary_c_string_t binary_c_zoo_animal_dog_eat_get(binary_c_zoo_animal_dog_pt msg,int i)
{
	return ((binary_c_string_t*)(msg->eat.data))[i];
}
void binary_c_zoo_animal_dog_cat_set(binary_c_zoo_animal_dog_pt msg,binary_c_zoo_animal_cat_pt val)
{
	msg->cat = val;
}
binary_c_zoo_animal_cat_pt binary_c_zoo_animal_dog_cat_get(binary_c_zoo_animal_dog_pt msg)
{
	return (binary_c_zoo_animal_cat_pt)(msg->cat);
}

void binary_c_zoo_animal_fish_reset(binary_c_zoo_animal_fish_pt msg)
{
	if (msg)
	{
		memset(msg, 0, sizeof(binary_c_zoo_animal_fish_t));
	}
}
void binary_c_zoo_animal_fish_free(binary_c_free_f free, binary_c_zoo_animal_fish_pt msg)
{
	if (msg)
	{
		if (free)
		{
		}
		memset(msg, 0, sizeof(binary_c_zoo_animal_fish_t));
	}
}
int binary_c_zoo_animal_fish_marshal_size(binary_c_context_pt ctx, binary_c_zoo_animal_fish_pt msg, int use)
{
	if (!msg)
	{
		return use;
	}

	return use;
}
int binary_c_zoo_animal_fish_marshal(binary_c_context_pt ctx, binary_c_zoo_animal_fish_pt msg, uint8_t *buffer, int capacity, int use)
{
	if(!msg)
	{
		return use;
	}
	return use;
}
int binary_c_zoo_animal_fish_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use)
{
	return 0;
}
int binary_c_zoo_animal_fish_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_zoo_animal_fish_pt msg)
{
	int ok = BINARY_C_OK;
	return ok;
}


