#ifndef __BINARY_C_C_BINARY_H__
#define __BINARY_C_C_BINARY_H__

#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define BINARY_C_DEFAULT_MAX_BUFFER (1024 * 50)

//  最大 id
#define BINARY_C_MAX_FIELD_ID 0x1FFF
#define BINARY_C_MAX_UINT16 0xFFFF
// byte bool
#define BINARY_C_WRITE_8 1
// int16 uint16 enum
#define BINARY_C_WRITE_16 2
// float32 int32 uint32
#define BINARY_C_WRITE_32 3
// float64 int64 uint64
#define BINARY_C_WRITE_64 4
// bytes strings type repeat
#define BINARY_C_WRITE_LENGTH 5
// bit repeat
#define BINARY_C_WRITE_BITS 6

#ifndef BINARY_C_FLOAT32_T
#define BINARY_C_FLOAT32_T float
#endif

#ifndef BINARY_C_FLOAT64_T
#define BINARY_C_FLOAT64_T double
#endif

// 成功
#define BINARY_C_OK 0

// 申請內存失敗
#define BINARY_C_ERROR_MALLOC -10
// 編碼長度 超過 uint16 限定
#define BINARY_C_ERROR_MAX_BUFFER -20
// 消息超過最大長度
#define BINARY_C_ERROR_MESSAGE_TOO_LONG -100

// 無法解開 field
#define BINARY_C_ERROR_UNMARSHAL_FIELD -300

// 未知 field 寫入類型
#define BINARY_C_ERROR_UNMARSHAL_FIELD_UNKNOW_WRITE_TYPE -301

// field 長度不匹配
#define BINARY_C_ERROR_UNMARSHAL_FIELD_LENGTH -302

#define BINARY_C_HAS_ERROR(val) (val < BINARY_C_OK)

#define BINARY_C_CHECK_ERROR(v) \
  if (v < 0)                    \
    return v;

#define BINARY_C_CHECK_ERROR_AND_FREE_CLASS(v, func, allocator, length, p)    \
  if (v < 0)                                                                  \
  {                                                                           \
    binary_c_free_class((binary_c_free_class_f)(func), allocator, length, p); \
    return v;                                                                 \
  }

#define BINARY_C_PUT_ID(ctx, buffer, id, tag) ctx->byte_order.put_uint16(buffer, id | (tag << 13));
#define BINARY_C_GET_ID(tag) (tag >> 13)

#ifdef __cplusplus
extern "C"
{
#endif
  /*
 * 類型 定義
*/
  typedef void *binary_c_interface_t;
#define BINARY_C_BOOL uint8_t
#define BINARY_C_TRUE 1
#define BINARY_C_FALSE 0
#define BINARY_C_IS_TRUE(val) (val == BINARY_C_TRUE)
#define BINARY_C_IS_FALSE(val) (val == BINARY_C_FALSE)

  typedef uint16_t (*binary_c_uint16_f)(const uint8_t *data);
  typedef uint32_t (*binary_c_uint32_f)(const uint8_t *data);
  typedef uint64_t (*binary_c_uint64_f)(const uint8_t *data);
  typedef void (*binary_c_put_uint16_f)(uint8_t *data, uint16_t val);
  typedef void (*binary_c_put_uint32_f)(uint8_t *data, uint32_t val);
  typedef void (*binary_c_put_uint64_f)(uint8_t *data, uint64_t val);
  /**
   * 字節編碼 序列
   */
  typedef struct
  {
    binary_c_uint16_f uint16;
    binary_c_uint32_f uint32;
    binary_c_uint64_f uint64;
    binary_c_put_uint16_f put_uint16;
    binary_c_put_uint32_f put_uint32;
    binary_c_put_uint64_f put_uint64;
  } binary_c_byte_order_t, *binary_c_byte_order_pt;
  /**
   * 初始化 小端序列
   */
  void binary_c_little_endian(binary_c_byte_order_pt byte_order);
  /**
   * 初始化 大端序列
   */
  void binary_c_big_endian(binary_c_byte_order_pt byte_order);

  /**
   * 編碼環境定義
   */
  typedef struct
  {
    /**
     * 支持的 最大編碼 長度
     */
    int max_buffer;

    /**
     * 字節編碼 序列
     */
    binary_c_byte_order_t byte_order;
  } binary_c_context_t, *binary_c_context_pt;

  /**
   * 返回默認的 編碼環境
   */
  binary_c_context_t binary_c_background();

  /**
   * 字符串定義
   */
  typedef struct
  {
    /**
     * 字符串 內存
    */
    char *data;
    /**
     * 字符串 長度
    */
    size_t length;
    /**
     * 字符串 容量
    */
    size_t capacity;
  } binary_c_string_t, *binary_c_string_pt;
  /**
   * 由 c 字符串 創建 字符串 對象
   */
  binary_c_string_t binary_c_make_const_string(const char *str);
  binary_c_string_t binary_c_make_string(char *str);
  BINARY_C_BOOL binary_c_is_string_equal(binary_c_string_pt s0, binary_c_string_pt s1);

  /**
   * 定義數組
   */
  typedef struct
  {
    /**
     * 數組 內存
    */
    binary_c_interface_t data;
    /**
     * 數組 長度
    */
    size_t length;
    /**
     * 數組 容量 如果爲0 則爲 const 容量
    */
    size_t capacity;
  } binary_c_array_t, *binary_c_array_pt;

  /**
   * 字節 數組
   * 
   * \param length bit 有效 長度
   * \param capacity bit 最大長度
   */
  typedef struct
  {
    uint8_t *data;
    size_t length;
    size_t capacity;
  } binary_c_bits_t, *binary_c_bits_pt;
  BINARY_C_BOOL binary_c_bits_get(binary_c_bits_pt bits, int i);
  BINARY_C_BOOL binary_c_bit_get(uint8_t val, int i);
  int binary_c_bits_set(binary_c_bits_pt bits, int i, BINARY_C_BOOL ok);
  int binary_c_bit_set(uint8_t *val, int i, BINARY_C_BOOL ok);

  typedef void *(*binary_c_malloc_f)(size_t);
  typedef void (*binary_c_free_f)(void *);
  /**
   * 內存分配器 定義了解碼時如何 分配內存 
   */
  typedef struct
  {
    /**
     * 內存申請函數
    */
    binary_c_malloc_f malloc;
    /**
     * 內存釋放函數
     */
    binary_c_free_f free;

    /**
     * 緩衝區
     * 
     * capacity > 0 使用 buffer 提供的內存 否則使用 malloc free 動態創建
     */
    binary_c_array_t buffer;
  } binary_c_allocator_t, *binary_c_allocator_pt;
  binary_c_allocator_t binary_c_make_static_allocator(binary_c_array_pt buffer);
  binary_c_allocator_t binary_c_make_static_allocator2(binary_c_interface_t buffer, size_t capacity);
  binary_c_allocator_t binary_c_make_dynamic_allocator(binary_c_malloc_f malloc, binary_c_free_f free);
  binary_c_interface_t binary_c_malloc(binary_c_allocator_pt allocator, size_t size);
  void binary_c_free(binary_c_allocator_pt allocator, binary_c_interface_t p, int length);
  void binary_c_free_string(binary_c_free_f free, binary_c_string_pt str);
  void binary_c_free_strings(binary_c_free_f free, binary_c_array_pt strs);
  void binary_c_free_array(binary_c_free_f free, binary_c_array_pt arrs);
  void binary_c_free_bits(binary_c_free_f free, binary_c_bits_pt bits);
  typedef void (*binary_c_free_class_f)(binary_c_free_f, binary_c_interface_t);
  void binary_c_free_class_array(binary_c_free_class_f free_class, binary_c_free_f free, binary_c_array_pt arrs);
  void binary_c_free_class(binary_c_free_class_f free_class, binary_c_allocator_pt allocator, int length, binary_c_interface_t p);

  int binary_c_marshal_size_add(binary_c_context_pt ctx, int use, int add);
  int binary_c_marshal_int16_size(binary_c_context_pt ctx, int use, int16_t v);
  int binary_c_marshal_int32_size(binary_c_context_pt ctx, int use, int32_t v);
  int binary_c_marshal_int64_size(binary_c_context_pt ctx, int use, int64_t v);
  int binary_c_marshal_uint16_size(binary_c_context_pt ctx, int use, uint16_t v);
  int binary_c_marshal_uint32_size(binary_c_context_pt ctx, int use, uint32_t v);
  int binary_c_marshal_uint64_size(binary_c_context_pt ctx, int use, uint64_t v);
  int binary_c_marshal_float32_size(binary_c_context_pt ctx, int use, BINARY_C_FLOAT32_T v);
  int binary_c_marshal_float64_size(binary_c_context_pt ctx, int use, BINARY_C_FLOAT64_T v);
  int binary_c_marshal_byte_size(binary_c_context_pt ctx, int use, uint8_t v);
  int binary_c_marshal_bool_size(binary_c_context_pt ctx, int use, BINARY_C_BOOL v);
  int binary_c_marshal_string_size(binary_c_context_pt ctx, int use, binary_c_string_pt v);

  int binary_c_marshal_int16s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_int32s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_int64s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_uint16s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_uint32s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_uint64s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_float32s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_float64s_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_bytes_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  int binary_c_marshal_bools_size(binary_c_context_pt ctx, int use, binary_c_bits_pt arrs);
  int binary_c_marshal_strings_size(binary_c_context_pt ctx, int use, binary_c_array_pt arrs);
  typedef int (*binary_c_marshal_size_f)(binary_c_context_pt ctx, binary_c_interface_t p, int use);
  int binary_c_marshal_class_array_size(binary_c_context_pt ctx, binary_c_marshal_size_f marshal_size, binary_c_array_pt arrs, int use);

  int binary_c_marshal_add(binary_c_context_pt ctx, int use, int add, int capacity);
  int binary_c_marshal_int16(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, int16_t v);
  int binary_c_marshal_int32(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, int32_t v);
  int binary_c_marshal_int64(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, int64_t v);
  int binary_c_marshal_uint16(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint16_t v);
  int binary_c_marshal_uint32(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint32_t v);
  int binary_c_marshal_uint64(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint64_t v);
  int binary_c_marshal_float32(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, BINARY_C_FLOAT32_T v);
  int binary_c_marshal_float64(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, BINARY_C_FLOAT64_T v);
  int binary_c_marshal_byte(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, uint8_t v);
  int binary_c_marshal_bool(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, BINARY_C_BOOL v);
  int binary_c_marshal_string(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_string_pt v);

  int binary_c_marshal_int16s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_int32s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_int64s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_uint16s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_uint32s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_uint64s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_float32s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_float64s(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_bytes(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  int binary_c_marshal_bools(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_bits_pt arrs);
  int binary_c_marshal_strings(binary_c_context_pt ctx, uint8_t *buffer, int capacity, int use, uint16_t id, binary_c_array_pt arrs);
  typedef int (*binary_c_marshal_f)(binary_c_context_pt ctx, binary_c_interface_t p, uint8_t *buffer, int capacity, int use);
  int binary_c_marshal_class_array(binary_c_context_pt ctx, binary_c_marshal_f marshal, binary_c_array_pt arrs, uint8_t *buffer, int capacity, int use);

  /**
   * 編碼後的 屬性信息
   */
  typedef struct
  {
    uint16_t id;
    uint8_t *data;
    int length;
  } binary_c_field_t, *binary_c_field_pt;

  int binary_c_unmarshal_field(binary_c_context_pt ctx, uint8_t *buffer, int n, binary_c_field_pt field);
  int binary_c_unmarshal_fields(binary_c_context_pt ctx, uint8_t *buffer, int n, binary_c_field_pt fields, int count);
  int binary_c_unmarshal_count(binary_c_context_pt ctx, uint8_t *buffer, int n);

  int binary_c_unmarshal_string_size(binary_c_field_pt field, int use);

  int binary_c_unmarshal_array_size(binary_c_field_pt field, int val_size, int use);
  int binary_c_unmarshal_int16s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_int32s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_int64s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_uint16s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_uint32s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_uint64s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_float32s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_float64s_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_bytes_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_bools_size(binary_c_field_pt field, int use);
  int binary_c_unmarshal_strings_size(binary_c_context_pt ctx, binary_c_field_pt field, int use);
  typedef int (*binary_c_unmarshal_size_f)(binary_c_context_pt ctx, uint8_t *buffer, int n, int use);
  int binary_c_unmarshal_class_array_size(binary_c_context_pt ctx, binary_c_unmarshal_size_f unmarshal_size, int class_size, binary_c_field_pt field, int use);

  int binary_c_unmarshal_bit8(binary_c_context_pt ctx, uint8_t *buffer, int n, uint8_t *output);
  int binary_c_unmarshal_bit16(binary_c_context_pt ctx, uint8_t *buffer, int n, uint16_t *output);
  int binary_c_unmarshal_bit32(binary_c_context_pt ctx, uint8_t *buffer, int n, uint32_t *output);
  int binary_c_unmarshal_bit64(binary_c_context_pt ctx, uint8_t *buffer, int n, uint64_t *output);
  int binary_c_unmarshal_array_bit(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_array_pt arrs, int valSeize);
  int binary_c_unmarshal_bits(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_bits_pt bits);
  int binary_c_unmarshal_string(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_string_pt str);
  int binary_c_unmarshal_strings(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_array_pt strs);

  typedef int (*binary_c_unmarshal_class_f)(binary_c_context_pt, binary_c_allocator_pt, uint8_t *, int, binary_c_interface_t p);
  int binary_c_unmarshal_class(binary_c_unmarshal_class_f unmarshal_class, binary_c_context_pt ctx, binary_c_allocator_pt allocator, binary_c_field_pt field, binary_c_interface_t *p, size_t size);
  int binary_c_unmarshal_class_array(binary_c_unmarshal_class_f unmarshal_class, binary_c_free_class_f free_class, binary_c_context_pt ctx, binary_c_allocator_pt allocator, binary_c_field_pt field, binary_c_array_pt arrs, size_t size);
#ifdef __cplusplus
}
#endif
#endif // __BINARY_C_C_BINARY_H__