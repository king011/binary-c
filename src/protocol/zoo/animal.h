#ifndef __BINARY_C_PROTOCOL_ZOO_ANIMAL_H__
#define __BINARY_C_PROTOCOL_ZOO_ANIMAL_H__
#include <binary_c.h>
#ifdef __cplusplus
extern "C"
{
#endif

	#define BINARY_C_ZOO_ANIMAL_POS_ENUM_POOL 0
	#define BINARY_C_ZOO_ANIMAL_POS_ENUM_CAGE 10
	#define BINARY_C_ZOO_ANIMAL_POS_ENUM_STEP 11
	#define BINARY_C_ZOO_ANIMAL_POS_ENUM_STAIRS 12
	#define BINARY_C_ZOO_ANIMAL_POS_ENUM_CORRIDOR_P0 13
	#define BINARY_C_ZOO_ANIMAL_POS_ENUM_CORRIDOR_P1 14
	const char* binary_c_zoo_animal_pos_enum_string(uint16_t val);
	uint16_t binary_c_zoo_animal_pos_enum_val(const char* str);


	typedef struct
	{
		binary_c_string_t name; // 1
		binary_c_interface_t dog; // 2
		binary_c_interface_t cat; // 3
	} binary_c_zoo_animal_cat_t, *binary_c_zoo_animal_cat_pt;

	typedef struct
	{
		int64_t id; // 1
		binary_c_array_t eat; // 2
		binary_c_interface_t cat; // 3
	} binary_c_zoo_animal_dog_t, *binary_c_zoo_animal_dog_pt;

	typedef struct
	{
		uint8_t __compatible_c;
	} binary_c_zoo_animal_fish_t, *binary_c_zoo_animal_fish_pt;

	void binary_c_zoo_animal_cat_reset(binary_c_zoo_animal_cat_pt msg);
	void binary_c_zoo_animal_cat_free(binary_c_free_f free, binary_c_zoo_animal_cat_pt msg);
	int binary_c_zoo_animal_cat_marshal_size(binary_c_context_pt ctx, binary_c_zoo_animal_cat_pt msg, int use);
	int binary_c_zoo_animal_cat_marshal(binary_c_context_pt ctx, binary_c_zoo_animal_cat_pt msg, uint8_t *buffer, int capacity, int use);
	int binary_c_zoo_animal_cat_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use);
	int binary_c_zoo_animal_cat_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_zoo_animal_cat_pt msg);
	void binary_c_zoo_animal_cat_dog_set(binary_c_zoo_animal_cat_pt msg, binary_c_zoo_animal_dog_pt val);
	binary_c_zoo_animal_dog_pt binary_c_zoo_animal_cat_dog_get(binary_c_zoo_animal_cat_pt msg);
	void binary_c_zoo_animal_cat_cat_set(binary_c_zoo_animal_cat_pt msg, binary_c_zoo_animal_cat_pt val);
	binary_c_zoo_animal_cat_pt binary_c_zoo_animal_cat_cat_get(binary_c_zoo_animal_cat_pt msg);

	void binary_c_zoo_animal_dog_reset(binary_c_zoo_animal_dog_pt msg);
	void binary_c_zoo_animal_dog_free(binary_c_free_f free, binary_c_zoo_animal_dog_pt msg);
	int binary_c_zoo_animal_dog_marshal_size(binary_c_context_pt ctx, binary_c_zoo_animal_dog_pt msg, int use);
	int binary_c_zoo_animal_dog_marshal(binary_c_context_pt ctx, binary_c_zoo_animal_dog_pt msg, uint8_t *buffer, int capacity, int use);
	int binary_c_zoo_animal_dog_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use);
	int binary_c_zoo_animal_dog_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_zoo_animal_dog_pt msg);
	void binary_c_zoo_animal_dog_eat_set(binary_c_zoo_animal_dog_pt msg, int i, binary_c_string_t val);
	binary_c_string_t binary_c_zoo_animal_dog_eat_get(binary_c_zoo_animal_dog_pt msg, int i);
	void binary_c_zoo_animal_dog_cat_set(binary_c_zoo_animal_dog_pt msg, binary_c_zoo_animal_cat_pt val);
	binary_c_zoo_animal_cat_pt binary_c_zoo_animal_dog_cat_get(binary_c_zoo_animal_dog_pt msg);

	void binary_c_zoo_animal_fish_reset(binary_c_zoo_animal_fish_pt msg);
	void binary_c_zoo_animal_fish_free(binary_c_free_f free, binary_c_zoo_animal_fish_pt msg);
	int binary_c_zoo_animal_fish_marshal_size(binary_c_context_pt ctx, binary_c_zoo_animal_fish_pt msg, int use);
	int binary_c_zoo_animal_fish_marshal(binary_c_context_pt ctx, binary_c_zoo_animal_fish_pt msg, uint8_t *buffer, int capacity, int use);
	int binary_c_zoo_animal_fish_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use);
	int binary_c_zoo_animal_fish_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_zoo_animal_fish_pt msg);

#ifdef __cplusplus
}
#endif

#endif // __BINARY_C_PROTOCOL_ZOO_ANIMAL_H__
