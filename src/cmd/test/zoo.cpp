#include <gtest/gtest.h>
#include <zoo/animal.h>

TEST(CatTest, HandleNoneZeroInput)
{
    binary_c_context_t ctx = binary_c_background();

    // base
    binary_c_zoo_animal_cat_t cat;
    binary_c_zoo_animal_cat_reset(&cat);
    int size = binary_c_zoo_animal_cat_marshal_size(&ctx, &cat, 0);
    EXPECT_EQ(size, 0);
    EXPECT_EQ(cat.name.capacity, 0);
    EXPECT_EQ(cat.name.length, 0);
    EXPECT_FALSE(cat.name.data);

    const char *name = "this is cat";
    cat.name = binary_c_make_const_string(name);
    EXPECT_EQ(cat.name.capacity, 0);
    EXPECT_EQ(cat.name.length, strlen(name));
    EXPECT_EQ(cat.name.data, name);
    size = binary_c_zoo_animal_cat_marshal_size(&ctx, &cat, 0);
    EXPECT_EQ(size, cat.name.length + 4);

    // marshal
    uint8_t *data = (uint8_t *)malloc(size);
    EXPECT_TRUE(data);
    uint8_t *ptr = data;
    int ok = binary_c_zoo_animal_cat_marshal(&ctx, &cat, data, size, 0);
    EXPECT_EQ(size, ok);
    EXPECT_EQ(data, ptr);

    binary_c_zoo_animal_cat_free(NULL, &cat);
    // unmarshal
    binary_c_allocator_t allocator = binary_c_make_dynamic_allocator(malloc, free);
    ok = binary_c_zoo_animal_cat_unmarshal(&ctx, &allocator, data, size, &cat);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    EXPECT_EQ(std::string(cat.name.data, cat.name.length), name);
    EXPECT_EQ(cat.name.length, strlen(name));
    EXPECT_EQ(cat.name.length, cat.name.capacity);
    binary_c_zoo_animal_cat_free(free, &cat);

    char bytes[1500];
    int unmarshal_size = binary_c_zoo_animal_cat_unmarshal_size(&ctx, data, size, 0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    allocator = binary_c_make_static_allocator2(bytes, sizeof(bytes) / sizeof(char));
    ok = binary_c_zoo_animal_cat_unmarshal(&ctx, &allocator, data, size, &cat);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    EXPECT_EQ(allocator.buffer.length, unmarshal_size);
    EXPECT_EQ(std::string(cat.name.data, cat.name.length), name);
    EXPECT_EQ(cat.name.length, strlen(name));
    EXPECT_EQ(cat.name.length, cat.name.capacity);
    binary_c_zoo_animal_cat_free(NULL, &cat);

    free(data);
}

#define DOG_IS_RESET(dog)           \
    EXPECT_EQ(dog.id, 0);           \
    EXPECT_EQ(dog.eat.capacity, 0); \
    EXPECT_EQ(dog.eat.length, 0);   \
    EXPECT_FALSE(dog.eat.data);
TEST(DogTest, HandleNoneZeroInput)
{
    binary_c_context_t ctx = binary_c_background();

    // base
    binary_c_zoo_animal_dog_t dog;
    binary_c_zoo_animal_dog_reset(&dog);
    int size = binary_c_zoo_animal_dog_marshal_size(&ctx, &dog, 0);
    EXPECT_EQ(size, 0);
    DOG_IS_RESET(dog)

    // id
    int64_t id = std::rand() * std::rand();
    dog.id = id;
    size = binary_c_zoo_animal_dog_marshal_size(&ctx, &dog, 0);
    EXPECT_EQ(size, 2 + sizeof(dog.id));
    uint8_t *data = (uint8_t *)malloc(size);
    EXPECT_TRUE(data);
    int ok = binary_c_zoo_animal_dog_marshal(&ctx, &dog, data, size, 0);
    EXPECT_EQ(size, ok);
    binary_c_zoo_animal_dog_free(NULL, &dog);

    binary_c_allocator_t allocator = binary_c_make_dynamic_allocator(malloc, free);
    ok = binary_c_zoo_animal_dog_unmarshal(&ctx, &allocator, data, size, &dog);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    EXPECT_EQ(dog.id, id);
    binary_c_zoo_animal_dog_free(free, &dog);
    DOG_IS_RESET(dog)

    char bytes[1500];
    int unmarshal_size = binary_c_zoo_animal_dog_unmarshal_size(&ctx, data, size, 0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    allocator = binary_c_make_static_allocator2(bytes, sizeof(bytes) / sizeof(char));
    ok = binary_c_zoo_animal_dog_unmarshal(&ctx, &allocator, data, size, &dog);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    EXPECT_EQ(allocator.buffer.length, unmarshal_size);
    EXPECT_EQ(dog.id, id);
    binary_c_zoo_animal_dog_free(NULL, &dog);
    DOG_IS_RESET(dog)

    free(data);

    // eat
    binary_c_string_t eat[2] = {
        binary_c_make_const_string("eat 0"),
        binary_c_make_const_string("eat 1"),
    };
    dog.eat.data = &eat;
    dog.eat.capacity = 0;
    dog.eat.length = 2;
    size = binary_c_zoo_animal_dog_marshal_size(&ctx, &dog, 0);
    EXPECT_EQ(size, 4 + 2 + eat[0].length + 2 + eat[1].length);
    data = (uint8_t *)malloc(size);
    EXPECT_TRUE(data);
    ok = binary_c_zoo_animal_dog_marshal(&ctx, &dog, data, size, 0);
    EXPECT_EQ(ok, size);
    binary_c_zoo_animal_dog_free(NULL, &dog);
    DOG_IS_RESET(dog)

    allocator = binary_c_make_dynamic_allocator(malloc, free);
    ok = binary_c_zoo_animal_dog_unmarshal(&ctx, &allocator, data, size, &dog);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));

    EXPECT_EQ(dog.id, 0);
    EXPECT_EQ(dog.eat.length, 2);
    EXPECT_EQ(dog.eat.length, dog.eat.capacity);

    for (int i = 0; i < dog.eat.length; i++)
    {
        binary_c_string_t str = binary_c_zoo_animal_dog_eat_get(&dog, i);
        EXPECT_EQ(str.length, eat[i].length);
        EXPECT_EQ(str.length, str.capacity);
        EXPECT_TRUE(binary_c_is_string_equal(&str, &eat[i]));
    }

    binary_c_zoo_animal_dog_free(free, &dog);
    DOG_IS_RESET(dog)

    unmarshal_size = binary_c_zoo_animal_dog_unmarshal_size(&ctx, data, size, 0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    allocator = binary_c_make_static_allocator2(bytes, sizeof(bytes) / sizeof(char));
    ok = binary_c_zoo_animal_dog_unmarshal(&ctx, &allocator, data, size, &dog);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    EXPECT_EQ(allocator.buffer.length, unmarshal_size);

    EXPECT_EQ(dog.id, 0);
    EXPECT_EQ(dog.eat.length, 2);
    EXPECT_EQ(dog.eat.length, dog.eat.capacity);

    for (int i = 0; i < dog.eat.length; i++)
    {
        binary_c_string_t str = binary_c_zoo_animal_dog_eat_get(&dog, i);
        EXPECT_EQ(str.length, eat[i].length);
        EXPECT_EQ(str.length, str.capacity);
        EXPECT_TRUE(binary_c_is_string_equal(&str, &eat[i]));
    }

    binary_c_zoo_animal_dog_free(NULL, &dog);
    DOG_IS_RESET(dog)

    free(data);
    // id and eat
    id = std::rand() * std::rand();
    dog.id = id;
    dog.eat.data = &eat;
    dog.eat.capacity = 0;
    dog.eat.length = 2;
    size = binary_c_zoo_animal_dog_marshal_size(&ctx, &dog, 0);
    EXPECT_EQ(size, 2 + sizeof(dog.id) + 4 + 2 + eat[0].length + 2 + eat[1].length);

    data = (uint8_t *)malloc(size);
    EXPECT_TRUE(data);
    ok = binary_c_zoo_animal_dog_marshal(&ctx, &dog, data, size, 0);
    EXPECT_EQ(size, ok);
    binary_c_zoo_animal_dog_free(NULL, &dog);
    DOG_IS_RESET(dog)

    allocator = binary_c_make_dynamic_allocator(malloc, free);
    ok = binary_c_zoo_animal_dog_unmarshal(&ctx, &allocator, data, size, &dog);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));

    EXPECT_EQ(dog.id, id);
    EXPECT_EQ(dog.eat.length, 2);
    EXPECT_EQ(dog.eat.length, dog.eat.capacity);

    for (int i = 0; i < dog.eat.length; i++)
    {
        binary_c_string_t str = binary_c_zoo_animal_dog_eat_get(&dog, i);
        EXPECT_EQ(str.length, eat[i].length);
        EXPECT_EQ(str.length, str.capacity);
        EXPECT_TRUE(binary_c_is_string_equal(&str, &eat[i]));
    }

    binary_c_zoo_animal_dog_free(free, &dog);
    DOG_IS_RESET(dog)

    unmarshal_size = binary_c_zoo_animal_dog_unmarshal_size(&ctx, data, size, 0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    allocator = binary_c_make_static_allocator2(bytes, sizeof(bytes) / sizeof(char));
    ok = binary_c_zoo_animal_dog_unmarshal(&ctx, &allocator, data, size, &dog);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    EXPECT_EQ(allocator.buffer.length, unmarshal_size);

    EXPECT_EQ(dog.id, id);
    EXPECT_EQ(dog.eat.length, 2);
    EXPECT_EQ(dog.eat.length, dog.eat.capacity);

    for (int i = 0; i < dog.eat.length; i++)
    {
        binary_c_string_t str = binary_c_zoo_animal_dog_eat_get(&dog, i);
        EXPECT_EQ(str.length, eat[i].length);
        EXPECT_EQ(str.length, str.capacity);
        EXPECT_TRUE(binary_c_is_string_equal(&str, &eat[i]));
    }

    binary_c_zoo_animal_dog_free(NULL, &dog);
    DOG_IS_RESET(dog)

    free(data);
}
