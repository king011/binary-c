#pragma once
#include <binary_c.h>
#include <types.h>

void display_types(binary_c_types_types_t *types);
void set_types(binary_c_types_types_t &types);