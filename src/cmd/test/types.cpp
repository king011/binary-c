#include <gtest/gtest.h>
#include "../types.h"

#define ARRAY_IS_RESET(arrs)     \
    EXPECT_EQ(arrs.capacity, 0); \
    EXPECT_EQ(arrs.length, 0);   \
    EXPECT_FALSE(arrs.data);

#define TYPES_NE_VAL(t0, t1, name) (t0.name != t1.name)
bool is_equal(binary_c_context_pt ctx, binary_c_zoo_animal_cat_pt t0, binary_c_zoo_animal_cat_pt t1)
{
    if (t0 && t1)
    {
        return binary_c_is_string_equal(&(t0->name), &(t1->name));
    }
    return !binary_c_zoo_animal_cat_marshal_size(ctx, t0, 0) && !binary_c_zoo_animal_cat_marshal_size(ctx, t1, 0);
}
bool is_equal(binary_c_context_pt ctx, binary_c_zoo_animal_dog_pt t0, binary_c_zoo_animal_dog_pt t1)
{
    if (t0 && t1)
    {
        if (t0->eat.length != t1->eat.length || t0->id != t1->id)
        {
            return false;
        }
        for (size_t i = 0; i < t0->eat.length; i++)
        {
            binary_c_string_t s0 = binary_c_zoo_animal_dog_eat_get(t0, i);
            binary_c_string_t s1 = binary_c_zoo_animal_dog_eat_get(t1, i);
            if (!binary_c_is_string_equal(&s0, &s1))
            {
                return false;
            }
        }
    }
    else
    {
        return !binary_c_zoo_animal_dog_marshal_size(ctx, t0, 0) && !binary_c_zoo_animal_dog_marshal_size(ctx, t1, 0);
    }
    return true;
}
#define TYPES_CHECK_ARRAY_EQUAL(t0, t1, name, length)                                               \
    for (size_t i = 0; i < length; i++)                                                             \
        if (binary_c_types_types_##name##_get(&t0, i) != binary_c_types_types_##name##_get(&t1, i)) \
            return false;

bool is_equal(binary_c_context_pt ctx, binary_c_types_types_t &t0, binary_c_types_types_t &t1)
{
    return true;
    if (TYPES_NE_VAL(t0, t1, int16) ||
        TYPES_NE_VAL(t0, t1, int32) ||
        TYPES_NE_VAL(t0, t1, int64) ||
        TYPES_NE_VAL(t0, t1, uint16) ||
        TYPES_NE_VAL(t0, t1, uint32) ||
        TYPES_NE_VAL(t0, t1, uint64) ||
        TYPES_NE_VAL(t0, t1, float32) ||
        TYPES_NE_VAL(t0, t1, float64) ||
        TYPES_NE_VAL(t0, t1, b) ||
        TYPES_NE_VAL(t0, t1, ok) ||
        TYPES_NE_VAL(t0, t1, str.length) ||
        !is_equal(ctx, (binary_c_zoo_animal_cat_pt)(t0.cat), (binary_c_zoo_animal_cat_pt)(t1.cat)) ||
        !is_equal(ctx, (binary_c_zoo_animal_dog_pt)(t0.dog), (binary_c_zoo_animal_dog_pt)(t1.dog)) ||
        TYPES_NE_VAL(t0, t1, pos) ||
        TYPES_NE_VAL(t0, t1, int16s.length) ||
        TYPES_NE_VAL(t0, t1, int32s.length) ||
        TYPES_NE_VAL(t0, t1, int64s.length) ||
        TYPES_NE_VAL(t0, t1, uint16s.length) ||
        TYPES_NE_VAL(t0, t1, uint32s.length) ||
        TYPES_NE_VAL(t0, t1, uint64s.length) ||
        TYPES_NE_VAL(t0, t1, float32s.length) ||
        TYPES_NE_VAL(t0, t1, float64s.length) ||
        TYPES_NE_VAL(t0, t1, bs.length) ||
        TYPES_NE_VAL(t0, t1, oks.length) ||
        TYPES_NE_VAL(t0, t1, strs.length) ||
        TYPES_NE_VAL(t0, t1, cats.length) ||
        TYPES_NE_VAL(t0, t1, dogs.length) ||
        TYPES_NE_VAL(t0, t1, poss.length))
    {
        return false;
    }
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, int16s, t0.int16s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, int32s, t0.int32s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, int64s, t0.int64s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, uint16s, t0.uint16s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, uint32s, t0.uint32s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, uint64s, t0.uint64s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, float32s, t0.float32s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, float64s, t0.float64s.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, bs, t0.bs.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, oks, t0.oks.length)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, poss, t0.poss.length)

    for (size_t i = 0; i < t0.strs.length; i++)
    {
        binary_c_string_t s0 = binary_c_types_types_strs_get(&t0, i);
        binary_c_string_t s1 = binary_c_types_types_strs_get(&t1, i);
        if (!binary_c_is_string_equal(&s0, &s1))
        {
            return false;
        }
    }

    for (size_t i = 0; i < t0.cats.length; i++)
    {
        binary_c_zoo_animal_cat_pt cat0 = binary_c_types_types_cats_get(&t0, i);
        binary_c_zoo_animal_cat_pt cat1 = binary_c_types_types_cats_get(&t1, i);
        if (!is_equal(ctx, cat0, cat1))
        {
            return false;
        }
    }

    for (size_t i = 0; i < t0.dogs.length; i++)
    {
        binary_c_zoo_animal_dog_pt dog0 = binary_c_types_types_dogs_get(&t0, i);
        binary_c_zoo_animal_dog_pt dog1 = binary_c_types_types_dogs_get(&t1, i);
        if (!is_equal(ctx, dog0, dog1))
        {
            return false;
        }
    }

    return true;
}

#define TYPES_IS_RESET(types)      \
    EXPECT_EQ(types.int16, 0);     \
    EXPECT_EQ(types.int32, 0);     \
    EXPECT_EQ(types.int64, 0);     \
    EXPECT_EQ(types.uint16, 0);    \
    EXPECT_EQ(types.uint32, 0);    \
    EXPECT_EQ(types.uint64, 0);    \
    EXPECT_EQ(types.float32, 0);   \
    EXPECT_EQ(types.float64, 0);   \
    EXPECT_EQ(types.b, 0);         \
    EXPECT_EQ(types.ok, 0);        \
    ARRAY_IS_RESET(types.str)      \
    EXPECT_FALSE(types.cat);       \
    EXPECT_FALSE(types.dog);       \
    EXPECT_EQ(types.pos, 0);       \
    ARRAY_IS_RESET(types.int16s)   \
    ARRAY_IS_RESET(types.int32s)   \
    ARRAY_IS_RESET(types.int64s)   \
    ARRAY_IS_RESET(types.uint16s)  \
    ARRAY_IS_RESET(types.uint32s)  \
    ARRAY_IS_RESET(types.uint64s)  \
    ARRAY_IS_RESET(types.float32s) \
    ARRAY_IS_RESET(types.float64s) \
    ARRAY_IS_RESET(types.bs)       \
    ARRAY_IS_RESET(types.oks)      \
    ARRAY_IS_RESET(types.strs)     \
    ARRAY_IS_RESET(types.cats)     \
    ARRAY_IS_RESET(types.dogs)     \
    ARRAY_IS_RESET(types.poss)

TEST(TypesTest, HandleNoneZeroInput)
{
    binary_c_context_t ctx = binary_c_background();

    binary_c_types_types_t types0, types1;
    binary_c_types_types_reset(&types0);
    TYPES_IS_RESET(types0)
    set_types(types0);

    binary_c_types_types_reset(&types1);
    TYPES_IS_RESET(types1)
    set_types(types1);

    EXPECT_TRUE(is_equal(&ctx, types0, types1));

    // marshal
    int size = binary_c_types_types_marshal_size(&ctx, &types0, 0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(size));
    uint8_t *data = (uint8_t *)malloc(size);

    int n = binary_c_types_types_marshal(&ctx, &types0, data, size, 0);
    EXPECT_EQ(size, n);

    binary_c_types_types_free(free, &types0);
    TYPES_IS_RESET(types0)

    binary_c_allocator_t allocator = binary_c_make_dynamic_allocator(malloc, free);
    int ok = binary_c_types_types_unmarshal(&ctx, &allocator, data, size, &types0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(size));

    EXPECT_TRUE(is_equal(&ctx, types0, types1));
    binary_c_types_types_free(free, &types0);
    TYPES_IS_RESET(types0)

    char bytes[1500];
    int unmarshal_size = binary_c_types_types_unmarshal_size(&ctx, data, size, 0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    allocator = binary_c_make_static_allocator2(bytes, sizeof(bytes) / sizeof(char));
    ok = binary_c_types_types_unmarshal(&ctx, &allocator, data, size, &types0);
    EXPECT_FALSE(BINARY_C_HAS_ERROR(ok));
    EXPECT_EQ(allocator.buffer.length, unmarshal_size);

    EXPECT_TRUE(is_equal(&ctx, types0, types1));
    binary_c_types_types_free(NULL, &types0);
    TYPES_IS_RESET(types0)

    free(data);

    binary_c_types_types_free(free, &types1);
    TYPES_IS_RESET(types1)
}