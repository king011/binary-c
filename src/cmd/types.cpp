#include "cmd.h"

#include "types.h"

void set_types(binary_c_types_types_t &types)
{
    types.int16 = -16;
    types.int32 = -32;
    types.int64 = -64;
    types.uint16 = 16;
    types.uint32 = 32;
    types.uint64 = 64;
    types.float32 = 32.32f;
    types.float64 = 64.64;
    types.b = 8;
    types.ok = 1;
    types.str = binary_c_make_const_string("cerberus is an idea");
    binary_c_zoo_animal_cat_pt cat = (binary_c_zoo_animal_cat_pt)malloc(sizeof(binary_c_zoo_animal_cat_t));
    binary_c_zoo_animal_cat_reset(cat);
    types.cat = cat;
    binary_c_types_types_cat_get(&types)->name = binary_c_make_const_string("cat");
    {
        binary_c_zoo_animal_dog_pt dog = (binary_c_zoo_animal_dog_pt)malloc(sizeof(binary_c_zoo_animal_dog_t));
        binary_c_zoo_animal_dog_reset(dog);
        dog->id = 2;
        cat->dog = dog;
    }
    binary_c_zoo_animal_dog_pt dog = (binary_c_zoo_animal_dog_pt)malloc(sizeof(binary_c_zoo_animal_dog_t));
    binary_c_zoo_animal_dog_reset(dog);
    binary_c_types_types_dog_set(&types, dog);
    binary_c_types_types_dog_get(&types)->id = 1;
    dog->eat.capacity = 2;
    dog->eat.length = dog->eat.capacity;
    dog->eat.data = malloc(dog->eat.capacity * sizeof(binary_c_string_t));
    binary_c_zoo_animal_dog_eat_set(dog, 0, binary_c_make_const_string("dog0"));
    binary_c_zoo_animal_dog_eat_set(dog, 1, binary_c_make_const_string("dog1"));
    {
        binary_c_zoo_animal_cat_pt cat = (binary_c_zoo_animal_cat_pt)malloc(sizeof(binary_c_zoo_animal_cat_t));
        binary_c_zoo_animal_cat_reset(cat);
        cat->name = binary_c_make_const_string("dog->cat");
        dog->cat = cat;
    }
    types.pos = BINARY_C_ZOO_ANIMAL_POS_ENUM_CAGE;

    types.int16s.capacity = 16;
    types.int16s.length = types.int16s.capacity;
    types.int16s.data = malloc(types.int16s.capacity * sizeof(int16_t));
    for (int i = 0; i < types.int16s.length; i++)
    {
        binary_c_types_types_int16s_set(&types, i, (int16_t)(-16 + i));
    }
    types.int32s.capacity = 16;
    types.int32s.length = types.int32s.capacity;
    types.int32s.data = malloc(types.int32s.capacity * sizeof(int32_t));
    for (int i = 0; i < types.int32s.length; i++)
    {
        binary_c_types_types_int32s_set(&types, i, (int32_t)(-32 + i));
    }
    types.int64s.capacity = 16;
    types.int64s.length = types.int64s.capacity;
    types.int64s.data = malloc(types.int64s.capacity * sizeof(int64_t));
    for (int i = 0; i < types.int64s.length; i++)
    {
        binary_c_types_types_int64s_set(&types, i, (int64_t)(-48 + i));
    }

    types.uint16s.capacity = 16;
    types.uint16s.length = types.uint16s.capacity;
    types.uint16s.data = malloc(types.uint16s.capacity * sizeof(uint16_t));
    for (int i = 0; i < types.uint16s.length; i++)
    {
        binary_c_types_types_uint16s_set(&types, i, (uint16_t)(i + 1));
    }
    types.uint32s.capacity = 16;
    types.uint32s.length = types.uint32s.capacity;
    types.uint32s.data = malloc(types.uint32s.capacity * sizeof(uint32_t));
    for (int i = 0; i < types.uint32s.length; i++)
    {
        binary_c_types_types_uint32s_set(&types, i, (uint32_t)(i + 17));
    }
    types.uint64s.capacity = 16;
    types.uint64s.length = types.uint64s.capacity;
    types.uint64s.data = malloc(types.uint64s.capacity * sizeof(uint64_t));
    for (int i = 0; i < types.uint64s.length; i++)
    {
        binary_c_types_types_uint64s_set(&types, i, (uint64_t)(i + 33));
    }

    types.float32s.capacity = 2;
    types.float32s.length = types.float32s.capacity;
    types.float32s.data = malloc(types.float32s.capacity * sizeof(float));
    binary_c_types_types_float32s_set(&types, 0, 32.1);
    binary_c_types_types_float32s_set(&types, 1, 32.2);
    types.float64s.capacity = 2;
    types.float64s.length = types.float64s.capacity;
    types.float64s.data = malloc(types.float64s.capacity * sizeof(double));
    binary_c_types_types_float64s_set(&types, 0, 64.1);
    binary_c_types_types_float64s_set(&types, 1, 64.2);

    types.bs.capacity = 8;
    types.bs.length = types.bs.capacity;
    types.bs.data = malloc(types.bs.capacity * sizeof(uint8_t));
    for (int i = 0; i < types.bs.length; i++)
    {
        binary_c_types_types_bs_set(&types, i, (uint8_t)(i + 1));
    }
    types.oks.capacity = 65;
    types.oks.length = types.oks.capacity;
    types.oks.data = (uint8_t *)malloc((types.oks.capacity + 7) / 8 * sizeof(uint8_t));
    for (int i = 0; i < types.oks.length; i++)
    {
        binary_c_types_types_oks_set(&types, i, i % 2 != 0);
    }

    types.strs.capacity = 3;
    types.strs.length = types.strs.capacity;
    types.strs.data = malloc(types.strs.capacity * sizeof(binary_c_string_t));
    binary_c_types_types_strs_set(&types, 0, binary_c_make_const_string("str0"));
    binary_c_types_types_strs_set(&types, 1, binary_c_make_const_string(NULL));
    binary_c_types_types_strs_set(&types, 2, binary_c_make_const_string("str1"));

    types.cats.capacity = 2;
    types.cats.length = types.cats.capacity;
    types.cats.data = malloc(types.cats.capacity * sizeof(binary_c_zoo_animal_cat_pt));
    cat = (binary_c_zoo_animal_cat_pt)malloc(sizeof(binary_c_zoo_animal_cat_t));
    binary_c_zoo_animal_cat_reset(cat);
    cat->name = binary_c_make_const_string("cat0");
    binary_c_types_types_cats_set(&types, 0, cat);
    cat = (binary_c_zoo_animal_cat_pt)malloc(sizeof(binary_c_zoo_animal_cat_t));
    binary_c_zoo_animal_cat_reset(cat);
    cat->name = binary_c_make_const_string("cat1");
    binary_c_types_types_cats_set(&types, 1, cat);

    types.dogs.capacity = 4;
    types.dogs.length = types.dogs.capacity;
    types.dogs.data = malloc(types.dogs.capacity * sizeof(binary_c_zoo_animal_dog_pt));
    dog = (binary_c_zoo_animal_dog_pt)malloc(sizeof(binary_c_zoo_animal_dog_t));
    binary_c_zoo_animal_dog_reset(dog);
    dog->id = 1;
    dog->eat.capacity = 2;
    dog->eat.length = dog->eat.capacity;
    dog->eat.data = malloc(dog->eat.capacity * sizeof(binary_c_string_t));
    binary_c_zoo_animal_dog_eat_set(dog, 0, binary_c_make_const_string("dog0"));
    binary_c_zoo_animal_dog_eat_set(dog, 1, binary_c_make_const_string("dog1"));
    binary_c_types_types_dogs_set(&types, 0, dog);
    binary_c_types_types_dogs_set(&types, 1, NULL);
    dog = (binary_c_zoo_animal_dog_pt)malloc(sizeof(binary_c_zoo_animal_dog_t));
    binary_c_zoo_animal_dog_reset(dog);
    dog->id = 2;
    dog->eat.capacity = 2;
    dog->eat.length = dog->eat.capacity;
    dog->eat.data = malloc(dog->eat.capacity * sizeof(binary_c_string_t));
    binary_c_zoo_animal_dog_eat_set(dog, 0, binary_c_make_const_string("dog2"));
    binary_c_zoo_animal_dog_eat_set(dog, 1, binary_c_make_const_string("dog3"));
    binary_c_types_types_dogs_set(&types, 2, dog);

    dog = (binary_c_zoo_animal_dog_pt)malloc(sizeof(binary_c_zoo_animal_dog_t));
    binary_c_zoo_animal_dog_reset(dog);
    binary_c_types_types_dogs_set(&types, 3, dog);

    types.poss.capacity = 2;
    types.poss.length = types.poss.capacity;
    types.poss.data = malloc(types.poss.capacity * sizeof(uint16_t));
    binary_c_types_types_poss_set(&types, 0, BINARY_C_ZOO_ANIMAL_POS_ENUM_CORRIDOR_P0);
    binary_c_types_types_poss_set(&types, 1, BINARY_C_ZOO_ANIMAL_POS_ENUM_CORRIDOR_P1);
}

int _g_malloc = 0;
void *local_malloc(size_t size)
{
    void *p = malloc(size);
    if (p)
    {
        _g_malloc += size;
    }
    return p;
}
void local_free(void *p)
{
    free(p);
}

void display_cat(binary_c_zoo_animal_cat_t *cat, const char *prefix = NULL);
void display_dog(binary_c_zoo_animal_dog_t *dog, const char *prefix = NULL);
void display_int16s(binary_c_types_types_t *types);
void display_int32s(binary_c_types_types_t *types);
void display_int64s(binary_c_types_types_t *types);
void display_uint16s(binary_c_types_types_t *types);
void display_uint32s(binary_c_types_types_t *types);
void display_uint64s(binary_c_types_types_t *types);
void display_float32s(binary_c_types_types_t *types);
void display_float64s(binary_c_types_types_t *types);
void display_bs(binary_c_types_types_t *types);
void display_oks(binary_c_types_types_t *types);
void display_strs(binary_c_types_types_t *types);
void display_cats(binary_c_types_types_t *types);
void display_dogs(binary_c_types_types_t *types);
void display_poss(binary_c_types_types_t *types);

void display_dog(binary_c_zoo_animal_dog_pt dog, const char *prefix)
{
    if (!dog)
    {
        return;
    }
    if (prefix)
    {
        printf("%s %10s : %ld\n", prefix, "dog.id", dog->id);
        printf("%s %10s : [", prefix, "dog.eat");
    }
    else
    {
        printf("%10s : %ld\n", "dog.id", dog->id);
        printf("%10s : [", "dog.eat");
    }
    if (dog->eat.length)
    {
        for (int i = 0; i < dog->eat.length; i++)
        {
            auto arr = binary_c_zoo_animal_dog_eat_get(dog, i);
            auto str = std::string(arr.data, arr.length);
            printf("%d=%s,", i, str.c_str());
        }
    }
    printf("]\n");
    if (dog->cat)
    {
        display_cat(binary_c_zoo_animal_dog_cat_get(dog), "dog-> ");
    }
}
void display_cat(binary_c_zoo_animal_cat_t *cat, const char *prefix)
{
    if (!cat)
    {
        return;
    }
    if (prefix)
    {
        printf("%s%10s : ", prefix, "cat.name");
    }
    else
    {
        printf("%10s : ", "cat.name");
    }
    std::cout << std::string(cat->name.data, cat->name.length) << "\n";
    if (cat->dog)
    {
        display_dog(binary_c_zoo_animal_cat_dog_get(cat), "cat-> ");
    }
}
void display_types(binary_c_types_types_t *types)
{
    printf("%10s : %d\n", "int16", types->int16);
    printf("%10s : %d\n", "int32", types->int32);
    printf("%10s : %ld\n", "int64", types->int64);
    printf("%10s : %d\n", "uint16", types->uint16);
    printf("%10s : %d\n", "uint32", types->uint32);
    printf("%10s : %ld\n", "uint64", types->uint64);
    printf("%10s : %f\n", "float32", types->float32);
    printf("%10s : %lf\n", "float64", types->float64);
    printf("%10s : %d\n", "b", types->b);
    printf("%10s : %d\n", "ok", types->ok);
    printf("%10s : ", "str");
    if (types->str.length)
    {
        std::cout << std::string(types->str.data, types->str.length) << "\n";
    }
    else
    {
        std::cout << "\n";
    }
    if (types->cat)
    {
        display_cat(binary_c_types_types_cat_get(types));
    }
    if (types->dog)
    {
        display_dog(binary_c_types_types_dog_get(types));
    }
    printf("%10s : ", "pos");
    std::cout << binary_c_zoo_animal_pos_enum_string(types->pos) << "\n";
    display_int16s(types);
    display_int32s(types);
    display_int64s(types);
    display_uint16s(types);
    display_uint32s(types);
    display_uint64s(types);
    display_float32s(types);
    display_float64s(types);
    display_bs(types);
    display_oks(types);
    display_strs(types);
    display_cats(types);
    display_dogs(types);
    display_poss(types);
    puts("");
}
void display_int16s(binary_c_types_types_t *types)
{
    if (types->int16s.length)
    {
        printf("%10s : [ ", "int16s");
        for (int i = 0; i < types->int16s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_int16s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "int16s");
    }
}
void display_int32s(binary_c_types_types_t *types)
{
    if (types->int32s.length)
    {
        printf("%10s : [ ", "int32s");
        for (int i = 0; i < types->int32s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_int32s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "int32s");
    }
}
void display_int64s(binary_c_types_types_t *types)
{
    if (types->int64s.length)
    {
        printf("%10s : [ ", "int64s");
        for (int i = 0; i < types->int64s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_int64s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "int64s");
    }
}
void display_uint16s(binary_c_types_types_t *types)
{
    if (types->uint16s.length)
    {
        printf("%10s : [ ", "uint16s");
        for (int i = 0; i < types->uint16s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_uint16s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "uint16s");
    }
}
void display_uint32s(binary_c_types_types_t *types)
{
    if (types->uint32s.length)
    {
        printf("%10s : [ ", "uint32s");
        for (int i = 0; i < types->uint32s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_uint32s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "uint32s");
    }
}
void display_uint64s(binary_c_types_types_t *types)
{
    if (types->uint64s.length)
    {
        printf("%10s : [ ", "uint64s");
        for (int i = 0; i < types->uint64s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_uint64s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "uint64s");
    }
}
void display_float32s(binary_c_types_types_t *types)
{
    if (types->float32s.length)
    {
        printf("%10s : [ ", "float32s");
        for (int i = 0; i < types->float32s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_float32s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "float32s");
    }
}
void display_float64s(binary_c_types_types_t *types)
{
    if (types->float64s.length)
    {
        printf("%10s : [ ", "float64s");
        for (int i = 0; i < types->float64s.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << binary_c_types_types_float64s_get(types, i);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "float64s");
    }
}
void display_bs(binary_c_types_types_t *types)
{
    if (types->bs.length)
    {
        printf("%10s : [ ", "bs");
        for (int i = 0; i < types->bs.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            printf("%d", binary_c_types_types_bs_get(types, i));
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "bs");
    }
}
void display_oks(binary_c_types_types_t *types)
{
    if (types->oks.length)
    {
        printf("%10s : [ ", "oks");
        for (int i = 0; i < types->oks.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            if (binary_c_types_types_oks_get(types, i))
            {
                std::cout << "true";
            }
            else
            {
                std::cout << "false";
            }
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "oks");
    }
}
void display_strs(binary_c_types_types_t *types)
{
    if (types->strs.length)
    {
        printf("%10s : [ ", "strs");
        for (int i = 0; i < types->strs.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            auto str = binary_c_types_types_strs_get(types, i);
            std::cout << std::string(str.data, str.length);
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "strs");
    }
}
void display_cats(binary_c_types_types_t *types)
{
    if (types->cats.length)
    {
        printf("%10s : [ ", "cats");
        for (int i = 0; i < types->cats.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            binary_c_zoo_animal_cat_pt cat = binary_c_types_types_cats_get(types, i);
            if (cat)
            {
                std::cout << std::string(cat->name.data, cat->name.length);
            }
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "cats");
    }
}
void display_dogs(binary_c_types_types_t *types)
{
    if (types->dogs.length)
    {
        printf("%10s : [ ", "dogs");
        for (int i = 0; i < types->dogs.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }

            std::cout << "{ ";

            binary_c_zoo_animal_dog_pt dog = binary_c_types_types_dogs_get(types, i);
            if (dog)
            {
                std::cout << dog->id << " : ";
                for (int j = 0; j < dog->eat.length; j++)
                {
                    if (j)
                    {
                        std::cout << " , ";
                    }
                    auto str = binary_c_zoo_animal_dog_eat_get(dog, j);
                    std::cout << std::string(str.data, str.length);
                }
            }
            std::cout << " }";
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "dogs");
    }
}
void display_poss(binary_c_types_types_t *types)
{
    if (types->poss.length)
    {
        printf("%10s : [ ", "poss");
        for (int i = 0; i < types->poss.length; i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            uint16_t v = binary_c_types_types_poss_get(types, i);
            std::cout << "{ "
                      << v << " : " << binary_c_zoo_animal_pos_enum_string(v)
                      << " }";
        }
        puts(" ]");
    }
    else
    {
        printf("%10s : \n", "poss");
    }
}
void subcommand_types(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("types", "test types");
    cmd->callback([&cmd = *cmd] {
        binary_c_context_t ctx = binary_c_background();

        puts("***   reset   ***");
        binary_c_types_types_t types;
        binary_c_types_types_reset(&types);
        display_types(&types);

        // set
        puts("***   set   ***");
        set_types(types);
        display_types(&types);

        puts("***   marshal   ***");
        int marshal_size = binary_c_types_types_marshal_size(&ctx, &types, 0);
        if (BINARY_C_HAS_ERROR(marshal_size))
        {
            std::cerr << "binary_c_types_types_marshal_size " << marshal_size << std::endl;
            throw CLI::RuntimeError(marshal_size);
        }
        std::cout << " marshal_size = " << marshal_size << "\n";
        uint8_t bytes[1500] = {0};

        int n = binary_c_types_types_marshal(&ctx, &types, bytes, 1500, 0);
        if (BINARY_C_HAS_ERROR(n))
        {
            std::cerr << "binary_c_types_types_marshal " << n << std::endl;
            throw CLI::RuntimeError(n);
        }
        if (n != marshal_size)
        {
            std::cerr << "n != marshal_size " << n << std::endl;
            throw CLI::RuntimeError(n);
        }

        puts("***   free   ***");
        binary_c_types_types_free(free, &types);
        display_types(&types);

        puts("***   unmarshal   ***");
        int unmarshal_size = binary_c_types_types_unmarshal_size(&ctx, bytes, marshal_size, 0);
        if (BINARY_C_HAS_ERROR(unmarshal_size))
        {
            std::cerr << "binary_c_types_types_unmarshal_size " << unmarshal_size << std::endl;
            throw CLI::RuntimeError(unmarshal_size);
        }
        std::cout << " unmarshal_size = " << unmarshal_size << "\n";
        binary_c_allocator_t allocator = binary_c_make_dynamic_allocator(local_malloc, local_free);
        n = binary_c_types_types_unmarshal(&ctx, &allocator, bytes, marshal_size, &types);
        if (BINARY_C_HAS_ERROR(n))
        {
            std::cerr << "binary_c_types_types_unmarshal " << n << std::endl;
            throw CLI::RuntimeError(n);
        }
        else if (_g_malloc != unmarshal_size)
        {
            std::cerr << "_g_malloc != unmarshal_size " << _g_malloc << std::endl;
            throw CLI::RuntimeError(n);
        }
        display_types(&types);

        puts("***   free   ***");
        binary_c_types_types_free(allocator.free, &types);
        display_types(&types);

        puts("***   static allocator   ***");
        binary_c_array_t buffer;
        uint8_t data[1500];
        buffer.capacity = sizeof(data) / sizeof(uint8_t);
        buffer.length = 0;
        buffer.data = data;
        allocator = binary_c_make_static_allocator(&buffer);
        n = binary_c_types_types_unmarshal(&ctx, &allocator, bytes, marshal_size, &types);
        if (BINARY_C_HAS_ERROR(n))
        {
            std::cerr << "binary_c_types_types_unmarshal " << n << std::endl;
            throw CLI::RuntimeError(n);
        }
        display_types(&types);
        if (allocator.buffer.length != unmarshal_size)
        {
            std::cerr << "buffer.length != unmarshal_size " << buffer.length << std::endl;
            throw CLI::RuntimeError(n);
        }

        std::cout
            << "success" << std::endl;
        // 通知 CLI11_PARSE 成功
        throw CLI::Success();
    });
}
