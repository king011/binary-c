#include "cmd.h"

// include 核心實現
#include <binary_c.h>
// include go-binary 工具創建的 代碼實現
#include <types.h>

void subcommand_example(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("example", "example");
    cmd->callback([&cmd = *cmd] {
        // 實例化一個 對象
        binary_c_types_types_t types;
        binary_c_types_types_reset(&types);
        // 初始化 對象 屬性
        {
            types.int16 = -16;                                             //基本類型
            types.ok = BINARY_C_TRUE;                                      // 布爾
            types.str = binary_c_make_const_string("cerberus is an idea"); // 字符串
            {
                // 另外一個 對象
                binary_c_zoo_animal_cat_pt cat = (binary_c_zoo_animal_cat_pt)malloc(sizeof(binary_c_zoo_animal_cat_t));
                binary_c_zoo_animal_cat_reset(cat);
                types.cat = cat;
            }
            types.pos = BINARY_C_ZOO_ANIMAL_POS_ENUM_CAGE; // 枚舉
        }
        // 初始化 數組屬性
        {
            // 基本類型
            types.int16s.capacity = 16;
            types.int16s.length = types.int16s.capacity;
            types.int16s.data = malloc(types.int16s.capacity * sizeof(int16_t));
            for (int i = 0; i < types.int16s.length; i++)
            {
                binary_c_types_types_int16s_set(&types, i, (int16_t)(-16 + i));
            }
            // 布爾
            types.oks.capacity = 65;
            types.oks.length = types.oks.capacity;
            types.oks.data = (uint8_t *)malloc((types.oks.capacity + 7) / 8 * sizeof(uint8_t));
            for (int i = 0; i < types.oks.length; i++)
            {
                binary_c_types_types_oks_set(&types, i, i % 2);
            }
            // 字符串
            types.strs.capacity = 3;
            types.strs.length = types.strs.capacity;
            types.strs.data = malloc(types.strs.capacity * sizeof(binary_c_string_t));
            binary_c_types_types_strs_set(&types, 0, binary_c_make_const_string("str0"));
            binary_c_types_types_strs_set(&types, 1, binary_c_make_const_string(NULL));
            binary_c_types_types_strs_set(&types, 2, binary_c_make_const_string("str1"));

            // 其它對象
            types.cats.capacity = 2;
            types.cats.length = types.cats.capacity;
            types.cats.data = malloc(types.cats.capacity * sizeof(binary_c_zoo_animal_cat_pt));
            binary_c_zoo_animal_cat_pt cat = (binary_c_zoo_animal_cat_pt)malloc(sizeof(binary_c_zoo_animal_cat_t));
            binary_c_zoo_animal_cat_reset(cat);
            binary_c_types_types_cats_set(&types, 0, cat);
            binary_c_types_types_cats_set(&types, 1, NULL);
            // 枚舉
            types.poss.capacity = 2;
            types.poss.length = types.poss.capacity;
            types.poss.data = malloc(types.poss.capacity * sizeof(uint16_t));
            binary_c_types_types_poss_set(&types, 0, BINARY_C_ZOO_ANIMAL_POS_ENUM_CORRIDOR_P0);
            binary_c_types_types_poss_set(&types, 1, BINARY_C_ZOO_ANIMAL_POS_ENUM_CORRIDOR_P1);
        }

        // 返回 默認的 編碼環境
        binary_c_context_t ctx = binary_c_background();

        // 編碼數據
        int marshal_size = binary_c_types_types_marshal_size(&ctx, &types, 0);
        uint8_t *data = (uint8_t *)malloc(marshal_size);
        if (!data)
        {
            throw CLI::RuntimeError(-1);
        }
        int ok = binary_c_types_types_marshal(&ctx, &types, data, marshal_size, 0);
        if (ok < 0 || ok != marshal_size)
        {
            throw CLI::RuntimeError(ok);
        }

        // 釋放 對象資源
        // 會在資源釋放後 自動調用 reset 函數
        binary_c_types_types_free(free, &types);
        // 解碼數據 使用 動態內存
        {
            binary_c_allocator_t allocator = binary_c_make_dynamic_allocator(malloc, free);
            ok = binary_c_types_types_unmarshal(&ctx, &allocator, data, marshal_size, &types);
            if (BINARY_C_HAS_ERROR(ok))
            {
                throw CLI::RuntimeError(ok);
            }

            binary_c_types_types_free(free, &types);
        }
        // 解碼數據 使用 靜態內存
        {
            uint8_t bytes[1500];
            int unmarshal_size = binary_c_types_types_unmarshal_size(&ctx, data, marshal_size, 0);
            if (unmarshal_size > 1500)
            {
                throw CLI::RuntimeError(-2);
            }
            binary_c_allocator_t allocator = binary_c_make_static_allocator2(bytes, 1500);
            ok = binary_c_types_types_unmarshal(&ctx, &allocator, data, marshal_size, &types);
            if (BINARY_C_HAS_ERROR(ok))
            {
                throw CLI::RuntimeError(ok);
            }
            if (allocator.buffer.length != unmarshal_size)
            {
                throw CLI::RuntimeError(allocator.buffer.length);
            }

            // 靜態內存 只能使用 NULL 調用 free
            // 當第一個參數爲 NULL free 只是簡單的調用 reset 函數
            binary_c_types_types_free(NULL, &types);
        }

        free(data);
        throw CLI::Success();
    });
}